Symulator PRT
Mateusz Krakowiak, 306297

1. Uruchamianie
Plik PRT.sln powinien da� si� otworzy� przez VS2010.
Korzystam z biblioteki ZedGraph, ale jest ona za��czona i wpisana do projektu.

2. Obs�uga programu
Powinna by� intuicyjna, ale:
- Drogi mog� by� tylko poziome / pionowe
- Punkty zaznaczone na niebiesko to skrzy�owania. Droga to odcinek ��cz�cy 2 skrzy�owania i maj�ca kierunek.
  W szczeg�lno�ci, droga mo�e mie� d�ugo�� 1 i nie b�dzie jej wida� (ale b�dzie istnie�).
- Prawy przycisk myszy, po naci�ni�ciu, pozwala na przesuwanie mapy.
- K�ko myszki pozwala przybli�a� / oddala�.
- Zawsze mo�na wybra� opcj� Info, by dowiedzie� si� np. o kierunku drogi.
- Przyk�adowe mapy s� w folderze bin/Debug. (podczas ich zapisywania by� bug w kodzie i nie b�dzie wida�
  zmiany koloru skrzy�owania po klikni�ciu na skrzy�owanie - modyfikacja save'�w w tej chwili jest praktycznie niemo�liwa)
- Klikni�cie na element trasy lub pojazd (w trakcie symulacji) wy��cza go z ruchu na 30 minut.
- Wszystkie gara�e i wszystkie stacje musz� si� widzie�, by rozpocz�� symulacj�.
- Pojazdy pe�ne s� czarne, puste - czerwone.

3. Uwagi z testowania
- Stacje typu FIFO s� bardzo niewydajne.
- Dynamiczna Dijkstra jest fajna bo pojazdy unikaj� kork�w (wida� np. na mapie big.prt)
- Odst�py mi�dzy pojazdami s� ca�kowicie niepotrzebne, ale, dla kompletno�ci, mo�na sobie je ustali� (dla ka�dej trasy).
- Dla du�ych map performance jest s�aby, bo za ka�dym razem, gdy pojazd wje�d�a na skrzy�owanie, przelicza
  najkr�tsz� �cie�k� do celu.
- Przycisk Stop nie modyfikuje mapy, tzn. symulacj� mo�na zrestartowa� bez problemu z innymi parametrami.
- Dla por�wania parametr�w symulacji, mo�na otworzy� okno statystyk i kopiuj-wklei� podsumowanie do notatnika.
