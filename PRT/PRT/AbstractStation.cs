﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public abstract class AbstractStation
    {
        protected AbstractStation(int size)
        {
            if (size <= 0)
                throw new Exception("Size must be positive");
            Size = size;
            StationRoutes = new List<StationRoute>();
            PassengerNodes = new List<PassengerNode>();
            InternalStationNodes = new List<InternalStationNode>();
            ExternalStationNodes = new List<ExternalStationNode>();
            PassengersWaiting = new Dictionary<AbstractStation, int>();
            NearestOutgoingDepots = new List<Depot>();
            NearestIncomingDepots = new List<Depot>();
            VehiclesCalledFromDepot = 0;
        }

        public void DeleteMe(Dictionary<Point, InfrastructureElement> elements)
        {
            foreach (var stationRoute in StationRoutes)
            {
                stationRoute.DeleteOnlyMe(elements);
            }
            StationRoutes.Clear();

            foreach (var passengerNode in PassengerNodes)
            {
                passengerNode.DeleteOnlyMe(elements);
            }
            PassengerNodes.Clear();

            foreach (var stationNode in InternalStationNodes) {
                stationNode.DeleteOnlyMe(elements);
            }
            InternalStationNodes.Clear();

            foreach (var stationNode in ExternalStationNodes) {
                stationNode.DeleteOnlyMe(elements);
            }
            ExternalStationNodes.Clear();

            if (Entrance != null)
                Entrance.DeleteOnlyMe(elements);
            if (Exit != null)
                Exit.DeleteOnlyMe(elements);
        }

        public void InitSimulation(List<AbstractStation> stations, List<Depot> depots, SimulationSettings settings)
        {
            // check if every depot has a path to us
            var depotsSeeingUs = settings.Pathfinder.NearestNodes(Entrance, typeof (Depot), true);
            if (depotsSeeingUs.Count != depots.Count)
            {
                depots.Sort();
                depotsSeeingUs.Sort();
                for (int i = 0; i < depots.Count; ++i)
                {
                    // find first difference in those lists
                    if (i >= depotsSeeingUs.Count || depotsSeeingUs[i] != depots[i])
                    {
                        throw new Exception("Station " + Entrance.Pos + " is unreachable from depot " + depots[i].Pos);
                    }
                }
            }
            // check if we have a path to every depot
            var depotsWeSee = settings.Pathfinder.NearestNodes(Exit, typeof (Depot), false);
            if (depotsWeSee.Count != depots.Count)
            {
                depots.Sort();
                depotsWeSee.Sort();
                for (int i = 0; i < depots.Count; ++i) {
                    // find first difference in those lists
                    if (i >= depotsWeSee.Count || depotsWeSee[i] != depots[i]) {
                        throw new Exception("Depot " + depots[i].Pos + " is unreachable from station " + Entrance.Pos);
                    }
                }
            }
            // check if we see every station
            var stationsWeSee = settings.Pathfinder.NearestNodes(Exit, typeof (StationEntrance), false);
            if (stationsWeSee.Count != stations.Count)
            {
                var stationEntrances = stations.Select(station => station.Entrance).ToList();
                stationEntrances.Sort();
                stationsWeSee.Sort();
                for (int i = 0; i < stationEntrances.Count; ++i) {
                    // find first difference in those lists
                    if (i >= stationsWeSee.Count || stationsWeSee[i] != stationEntrances[i]) {
                        throw new Exception("Station " + stationEntrances[i].Pos + " is unreachable from station " + Entrance.Pos);
                    }
                }
            }
            // nothrow now
            NearestOutgoingDepots.Clear();
            foreach (var abstractNode in depotsWeSee)
            {
                var depot = abstractNode as Depot;
                Debug.Assert(depot != null);
                NearestOutgoingDepots.Add(depot);
            }
            NearestIncomingDepots.Clear();
            foreach (var abstractNode in depotsSeeingUs) {
                var depot = abstractNode as Depot;
                Debug.Assert(depot != null);
                NearestIncomingDepots.Add(depot);
            }
            foreach (var station in stations.Where(station => station != this))
            {
                PassengersWaiting.Add(station, 0);
            }
        }

        public void ClearSimulation()
        {
            PassengersWaiting.Clear();
            NearestOutgoingDepots.Clear();
            NearestIncomingDepots.Clear();
            VehiclesCalledFromDepot = 0;
        }

        public void CreatePassengers(PassengerIncreaseChance chance, int hour, int constant, System.Random pseudoRandom)
        {
            var keys = new List<AbstractStation>(PassengersWaiting.Keys);
            foreach (var station in keys)
            {
                // adds new passengers with a chance for one
                // if chance >= 1, adds floor(chance) passengers + chance for one
                double myChance = chance(this, station, hour, constant);
                PassengersWaiting[station] += (int)Math.Floor(myChance);
                myChance -= Math.Floor(myChance);
                if (pseudoRandom.NextDouble() < myChance)
                    PassengersWaiting[station] += 1;
            }
        }

        // queues passengers in front of appropriate new vehicles
        public void AssignNewPassengers()
        { 
            // firstly assign to nodes at the back; for FIFO stations they will be able to start quicker
            for (int i = PassengerNodes.Count - 1; i >= 0; --i)
            {
                var dockedVehicle = PassengerNodes[i].DockedVehicle;
                if (dockedVehicle != null) {
                    if (dockedVehicle.PassengersDestination == null) {
                        // empty vehicle that needs destination binding
                        int maxWaiting = 0;
                        AbstractStation maxStation = null;
                        foreach (var kv in PassengersWaiting) {
                            if (kv.Value > maxWaiting) {
                                maxWaiting = kv.Value;
                                maxStation = kv.Key;
                            }
                        }
                        if (maxWaiting == 0) continue;
                        Debug.Assert(dockedVehicle.Passengers == 0 && dockedVehicle.QueuedPassengers == 0);
                        dockedVehicle.PassengersDestination = maxStation;
                        dockedVehicle.QueuedPassengers = Math.Min(dockedVehicle.Capacity, maxWaiting);
                        maxWaiting -= dockedVehicle.QueuedPassengers;
                        PassengersWaiting.Remove(maxStation);
                        PassengersWaiting.Add(maxStation, maxWaiting);
                    } else if (dockedVehicle.PassengersDestination != this) {
                        // try to add new passengers to an existing queue
                        int freeSeats = dockedVehicle.Capacity - (dockedVehicle.Passengers + dockedVehicle.QueuedPassengers);
                        if (freeSeats > 0) {
                            int passengersWaitingForUs = PassengersWaiting[dockedVehicle.PassengersDestination];
                            if (passengersWaitingForUs > 0) {
                                int newPassengers = Math.Min(passengersWaitingForUs, freeSeats);
                                PassengersWaiting[dockedVehicle.PassengersDestination] -= newPassengers;
                                dockedVehicle.QueuedPassengers += newPassengers;
                            }
                        }
                    }
                }
            }
        }

        public void CallForVehicles(SimulationSettings settings)
        {
            if (NumberOfVehiclesDocked() == Size)
                return; // station full
            if (VehiclesCalledFromDepot >= Size / 2)
                return; // too many vehicles trying to get to a station

            int passengersWaiting = NumberOfPassengersWaiting();
            double chanceForRequest = Math.Min(0.2, settings.ChanceToCallForVehicle*passengersWaiting/settings.VehicleCapacity);
            if (Random.Instance.NextDouble() <= chanceForRequest)
            {
                foreach (var depot in NearestIncomingDepots)
                {
                    if (depot.RequestVehicle(this))
                    {
                        ++VehiclesCalledFromDepot;
                        break;
                    }
                }
            }
        }

        public void SendVehiclesToDepot(SimulationSettings settings)
        {
            foreach (var passengerNode in PassengerNodes)
            {
                var dockedVehicle = passengerNode.DockedVehicle;
                if (dockedVehicle != null && dockedVehicle.PassengersDestination == null
                    && Random.Instance.NextDouble() < settings.ChanceToSendEmptyVehicleToDepot)
                {
                    dockedVehicle.DepotDestination = NearestOutgoingDepots.First();
                    passengerNode.StartVehicle();
                }
            }
        }

        public bool CanStart(Vehicle dockedVehicle, SimulationSettings settings)
        {
            return dockedVehicle.Passengers*100 >= dockedVehicle.Capacity*settings.VehicleFillPercentage;
        }

        // 1 passenger from each vehicle queue enters the vehicle
        public void LoadPassengers(SimulationSettings settings)
        {
            foreach(var passengerNode in PassengerNodes)
            {
                var dockedVehicle = passengerNode.DockedVehicle;
                if (dockedVehicle != null)
                {
                    if (dockedVehicle.PassengersDestination == null
                        || dockedVehicle.PassengersDestination == this)
                    {
                        // don't try to load if we haven't finished unloading
                        // or haven't bound destination station to vehicle
                        continue;
                    }
                    if (dockedVehicle.QueuedPassengers > 0)
                    {
                        ++dockedVehicle.Passengers;
                        --dockedVehicle.QueuedPassengers;
                    }
                    if (dockedVehicle.QueuedPassengers == 0 && CanStart(dockedVehicle, settings))
                        passengerNode.StartVehicle();
                }
            }
        }

        public void UnloadPassengers(SimulationSettings settings)
        {
            foreach(var passengerNode in PassengerNodes.Where(node =>
                node.DockedVehicle != null && node.DockedVehicle.PassengersDestination == this))
            {
                var dockedVehicle = passengerNode.DockedVehicle;
                dockedVehicle.Passengers = Math.Max(dockedVehicle.Passengers - 1, 0);
                if (dockedVehicle.Passengers == 0)
                    dockedVehicle.PassengersDestination = null; // ended unloading, waiting for load binding
            }
        }

        // returns null if nothing found
        public abstract PassengerNode FindFreePassengerNode();

        public int NumberOfPassengersWaiting()
        {
            return PassengersWaiting.Values.Sum();
        }

        public int NumberOfVehiclesDocked()
        {
            return PassengerNodes.Sum(node => node.DockedVehicle != null ? 1 : 0);
        }

        public StationEntrance Entrance { get; protected set; }
        public StationExit Exit { get; protected set; }
        public int Size { get; protected set; }

        // every node where a vehicle can take passengers
        // sorted in order of appearance, counting from station entrance
        public List<PassengerNode> PassengerNodes { get; protected set; }
        // other nodes which cannot be modified by user
        // both external and internal
        public List<InternalStationNode> InternalStationNodes { get; protected set; }
        public List<ExternalStationNode> ExternalStationNodes { get; protected set; }
        // routes connecting those nodes
        public List<StationRoute> StationRoutes { get; protected set; }

        public Dictionary<AbstractStation, int> PassengersWaiting { get; set; }
        protected List<Depot> NearestOutgoingDepots { get; set; }
        protected List<Depot> NearestIncomingDepots { get; set; }
        public int VehiclesCalledFromDepot { get; set; }
    }
}
