﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    class TerminusStation : AbstractStation
    {
        public TerminusStation(int size, Direction direction, Point pos, Dictionary<Point, InfrastructureElement> elements)
            : base(size)
        {
            var turtle = new Turtle(direction, pos);
            try
            {
                var begin = new ExternalStationNode(turtle.Position, this, elements);
                ExternalStationNodes.Add(begin);

                turtle.MoveForward(2);
                Entrance = new StationEntrance(turtle.Position, this, elements);
                StationRoutes.Add(new StationRoute(this, begin, Entrance, elements));

                turtle.RotateLeft();
                StationNode lastNode = Entrance;
                for (int i = 0; i < Size; ++i) {
                    turtle.MoveForward(2);
                    var junctionNode = new InternalStationNode(turtle.Position, this, elements);
                    InternalStationNodes.Add(junctionNode);
                    StationRoutes.Add(new StationRoute(this, lastNode, junctionNode, elements));
                    turtle.RotateRight();
                    turtle.MoveForward(2);
                    var passengerNode = new TerminusPassengerNode(turtle.Position, this, elements);
                    PassengerNodes.Add(passengerNode);
                    var baseRoute = new StationRoute(this, junctionNode, passengerNode, elements);
                    StationRoutes.Add(baseRoute);
                    // this route is two-way; I simulate it by adding invisible route backwards
                    StationRoutes.Add(new StationInvertedRoute(this, passengerNode, junctionNode, baseRoute));
                    turtle.RotateRight();
                    turtle.RotateRight();
                    turtle.MoveForward(2);
                    turtle.RotateRight();
                    lastNode = junctionNode;
                }
                turtle.MoveForward(2);
                Exit = new TerminusStationExit(turtle.Position, this, elements);
                StationRoutes.Add(new StationRoute(this, lastNode, Exit, elements));

                turtle.RotateLeft();
                turtle.MoveForward(2);
                var end = new ExternalStationNode(turtle.Position, this, elements);
                ExternalStationNodes.Add(end);
                StationRoutes.Add(new StationRoute(this, Exit, end, elements));
            } catch (Exception e) {
                DeleteMe(elements);
                throw new Exception("Cannot build station: " + e.Message);
            }
        }

        public override PassengerNode FindFreePassengerNode()
        {
            for (int i = PassengerNodes.Count - 1; i >= 0; --i)
            {
                if (!PassengerNodes[i].Reserved)
                {
                    PassengerNodes[i].Reserved = true;
                    return PassengerNodes[i];
                }
            }
            return null;
        }
    }
}
