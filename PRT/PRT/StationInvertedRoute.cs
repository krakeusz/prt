﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class StationInvertedRoute : StationRoute
    {
        public StationInvertedRoute(AbstractStation station, AbstractNode begin, AbstractNode end,
                                    StationRoute siblingRoute)
            : base(station, begin, end, null, InvertedRouteElement.Instance, null)
        {
            SiblingRoute = siblingRoute;
        }

        // the route that is over us and in opposite direction
        public StationRoute SiblingRoute { get; set; }
    }
}
