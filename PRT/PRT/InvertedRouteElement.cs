﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    // route element that belongs to an inverted route (another route under existing one, backwards)
    [Serializable]
    public class InvertedRouteElement : RouteElement
    {
        protected InvertedRouteElement(int index, Point pos, AbstractRoute route)
            : base (index, pos, route)
        {}

        public override RouteElement Create(int index, Point pos, AbstractRoute route)
        {
            return new InvertedRouteElement(index, pos, route);
        }

        [NonSerialized]
        protected static readonly new InvertedRouteElement instance = new InvertedRouteElement(0, new Point(), null);
        public static new RouteElement Instance { get { return instance; } }

        public override Vehicle Vehicle
        {
            get { return _vehicle; }
            set
            {
                base.Vehicle = value;
                // set color of sibling RouteElement
                var ourRoute = this.Route as StationInvertedRoute;
                Debug.Assert(ourRoute != null);
                var siblingElement = ourRoute.SiblingRoute.RouteElements[Route.RouteElements.Length - Index];
                Debug.Assert(siblingElement.Vehicle == null || value == null);
                siblingElement.Color = value == null ? siblingElement.DefaultColor : Color;
            }
        }
    }
}
