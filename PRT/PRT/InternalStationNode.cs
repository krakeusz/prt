﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    // currently used only by terminus station
    [Serializable]
    public class InternalStationNode : StationNode
    {
        public InternalStationNode(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
        }

        public override bool IsUserLinkable()
        {
            return false;
        }

        public override bool HasTrafficLight
        {
            get { return _hasTrafficLight; }
            set
            {
                _hasTrafficLight = false; // don't allow for traffic lights inside stations
            }
        }

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            if (movingVehicle.LastPassengerNode != null) {
                movingVehicle.LastPassengerNode.Reserved = false;
                movingVehicle.LastPassengerNode = null;
            }
            movingVehicle.Destination = Station.Exit;
        }
    }
}
