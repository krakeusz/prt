﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using ZedGraph;

namespace PRT
{
    public class StatisticsGraphs
    {
        // how many days must pass to draw next point of graphs
        private const int daysToPass = 5;

        public StatisticsGraphs(WindowsFormsHost host, TextBox shortStatsBox)
        {
            Host = host;
            ShortStatsBox = shortStatsBox;
            Stats = new List<Stat>();
        }

        public void AddSimulation(Simulation simulation)
        {
            Debug.Assert(Simulation == null);
            Simulation = simulation;
            GraphControl = new ZedGraphControl();
            GraphControl.MasterPane.PaneList.Clear();
            Host.Child = GraphControl;
            DaysPassedSinceDraw = 0;

            var passengersPane = new GraphPane();
            passengersPane.Title.Text = "Passengers";
            Stats.Add(new PassengersWaitingStat(Color.Red, passengersPane));
            Stats.Add(new PassengersInQueueStat(Color.Orange, passengersPane));
            Stats.Add(new PassengersInVehiclesStat(Color.Blue, passengersPane));
            GraphControl.MasterPane.PaneList.Add(passengersPane);
            
            var speedPane = new GraphPane();
            speedPane.Title.Text = "Vehicles' speed";
            Stats.Add(new MaxVehicleSpeedStat(Color.Brown, speedPane));
            Stats.Add(new AverageVehicleSpeedStat(Color.Green, speedPane));
            GraphControl.MasterPane.PaneList.Add(speedPane);

            var vehicleCountPane = new GraphPane();
            vehicleCountPane.Title.Text = "Vehicles' count";
            Stats.Add(new VehicleCountStat(Color.Brown, vehicleCountPane));
            Stats.Add(new ActiveVehicleCountStat(Color.Red, vehicleCountPane));
            GraphControl.MasterPane.PaneList.Add(vehicleCountPane);

            var loadPane = new GraphPane();
            loadPane.Title.Text = "Vehicles' load";
            Stats.Add(new MaxVehicleLoadStat(Color.Brown, loadPane));
            Stats.Add(new AverageVehicleLoadStat(Color.Blue, loadPane));
            GraphControl.MasterPane.PaneList.Add(loadPane);

            GraphControl.MasterPane.SetLayout(GraphControl.CreateGraphics(), PaneLayout.SquareRowPreferred);
            GraphControl.IsAntiAlias = true;

            foreach (var pane in GraphControl.MasterPane.PaneList)
            {
                pane.XAxis.Type = AxisType.Date;
                pane.XAxis.Title.IsVisible = false;
                pane.YAxis.Title.IsVisible = false;
            }
        }

        public void ClearSimulation()
        {
            Simulation = null;
            Host.Child = null;
            ShortStatsBox.Text = "";
            GraphControl = null;
            Stats.Clear();
            DaysPassedSinceDraw = 0;
        }

        public void SetShortStats()
        {
            String summary = "Simulation start: " + Simulation.Settings.StartingTime + "\n"
                           + "Time elapsed: " + (Simulation.SimulationTime - Simulation.Settings.StartingTime) + "\n"
                           + "\n"
                           + "Average stats from beginning:\n";
            summary = Stats.Aggregate(summary, (current, stat) => current + stat + "\n");
            summary += "\n"
                       + "" +
                       "Other stats:\n"
                       + "Acceleration: " + Simulation.Settings.VehicleA + "\n"
                       + "Fill % to start: " + Simulation.Settings.VehicleFillPercentage + "%\n"
                       + "Average passengers between every station per hour: " +
                       Simulation.Settings.PassengerIncreaseConstant + "\n"
                       + "Pathfinder: " + Simulation.Settings.Pathfinder + "\n"
                       + "Traffic lights: " + (Simulation.Settings.IsTrafficLight ? "On" : "Off") + "\n";
            if (Simulation.Settings.IsTrafficLight)
                summary += "Traffic lights' period: " + Simulation.Settings.TrafficLightPeriod + "\n";
            ShortStatsBox.Text = summary;
        }

        /// <summary>
        /// Creates new point on each graph, corresponding to actual Simulation time.
        /// </summary>
        public void DrawNewPoints()
        {
            ++DaysPassedSinceDraw;

            XDate currentDate = new XDate(Simulation.SimulationTime);
            
            foreach (var stat in Stats) {
                stat.OnTick(Simulation);
                if (DaysPassedSinceDraw >= daysToPass)
                    stat.DrawLastTick(currentDate);
            }

            if (DaysPassedSinceDraw >= daysToPass)
            {
                XDate minDate = new XDate(Simulation.SimulationTime.Subtract(new TimeSpan(0, 6, 0, 0)));
                XDate maxDate = new XDate(Simulation.SimulationTime.Add(new TimeSpan(0, 0, 5, 0)));
                foreach (var pane in GraphControl.MasterPane.PaneList)
                {
                    pane.XAxis.Scale.Min = minDate;
                    pane.XAxis.Scale.Max = maxDate;
                    double maxY = 0.0;
                    foreach (var curve in pane.CurveList)
                    {
                        for (int i = curve.Points.Count - 1; i >= 0 && curve.Points[i].X >= minDate; --i)
                        {
                            if (curve.Points[i].Y > maxY)
                                maxY = curve.Points[i].Y;
                        }
                    }
                    pane.YAxis.Scale.Max = maxY * 1.1;
                    pane.YAxis.Scale.Min = 0.0;
                }
                GraphControl.AxisChange();
                GraphControl.Refresh();
                DaysPassedSinceDraw = 0;
            }
            SetShortStats();
        }

        private Simulation Simulation { get; set; }
        private WindowsFormsHost Host { get; set; }
        private TextBox ShortStatsBox { get; set; }
        private ZedGraphControl GraphControl { get; set; }
        private List<Stat> Stats { get; set; } 
        private int DaysPassedSinceDraw { get; set; }
        
    }

    /// <summary>
    /// Records history of one stat (so that average can be counted).
    /// </summary>
    public abstract class Stat
    {
        protected Stat(String name, Color color, GraphPane parentPane)
        {
            Name = name;
            StatCurve = new LineItem(Name, null, null, color, SymbolType.None);
            parentPane.CurveList.Add(StatCurve);
            IsStatNull = true;
        }

        /// <summary>
        /// Retrieves stat from simulation, saves it in LastStat and updates sum and count.
        /// </summary>
        /// <param name="simulation"></param>
        public void OnTick(Simulation simulation)
        {
            RetrieveStat(simulation);
            if (!IsStatNull)
            {
                StatSum += LastStat;
                ++StatCount;
            }
    }

        public void DrawLastTick(XDate currentDate)
        {
            StatCurve.AddPoint(currentDate, LastStat);
        }

        /// <summary>
        /// Must set IsStatNull and, if false, LastStat.
        /// </summary>
        /// <param name="simulation"></param>
        protected abstract void RetrieveStat(Simulation simulation);

        public double Average()
        {
            if (StatCount == 0)
                return 0.0;
            return StatSum/StatCount;
        }

        public override string ToString()
        {
            return Name + ": " + Math.Round(Average(), 2);
        }

        public LineItem StatCurve { get; set; }
        public String Name { get; set; }
        public bool IsStatNull { get; set; }
        public Double LastStat { get; set; }
        private double StatSum { get; set; }
        private int StatCount { get; set; }
    }

    public class PassengersWaitingStat : Stat
    {
        public PassengersWaitingStat(Color color, GraphPane parentPane) 
            : base("Passengers waiting", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Map.Stations.Sum(station => station.NumberOfPassengersWaiting());
        }
    }

    public class PassengersInQueueStat : Stat
    {
        public PassengersInQueueStat(Color color, GraphPane parentPane)
            : base("Passengers entering vehicles", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Vehicles.Sum(vehicle => vehicle.QueuedPassengers);
        }
    }

    public class PassengersInVehiclesStat : Stat
    {
        public PassengersInVehiclesStat(Color color, GraphPane parentPane)
            : base("Passengers in vehicles", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Vehicles.Sum(vehicle => vehicle.Passengers);
        }
    }

    public class VehicleCountStat : Stat
    {
        public VehicleCountStat(Color color, GraphPane parentPane)
            : base("Total vehicles", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Vehicles.Count;
        }
    }

    public class ActiveVehicleCountStat : Stat
    {
        public ActiveVehicleCountStat(Color color, GraphPane parentPane)
            : base("Vehicles on track", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Vehicles.Sum(vehicle => vehicle.IsInDepot() ? 0 : 1);
        }
    }

    public class MaxVehicleSpeedStat : Stat
    {
        public MaxVehicleSpeedStat(Color color, GraphPane parentPane)
            : base("Max vehicle speed", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Settings.VehicleVMax;
        }
    }

    public class AverageVehicleSpeedStat : Stat
    {
        public AverageVehicleSpeedStat(Color color, GraphPane parentPane)
            : base("Average active vehicle speed", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            var activeVehicles = simulation.Vehicles.Where(vehicle => vehicle.Active).ToList();
            if (!activeVehicles.Any())
            {
                IsStatNull = true;
                return;
            }
            IsStatNull = false;
            LastStat = activeVehicles.Average(vehicle => (double)vehicle.V);
        }
    }

    public class MaxVehicleLoadStat : Stat
    {
        public MaxVehicleLoadStat(Color color, GraphPane parentPane)
            : base("Max vehicle load", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            IsStatNull = false;
            LastStat = simulation.Settings.VehicleCapacity;
        }
    }

    public class AverageVehicleLoadStat : Stat
    {
        public AverageVehicleLoadStat(Color color, GraphPane parentPane)
            : base("Average active vehicle load", color, parentPane)
        {
        }

        protected override void RetrieveStat(Simulation simulation)
        {
            var activeVehicles = simulation.Vehicles.Where(vehicle => vehicle.Active).ToList();
            if (!activeVehicles.Any()) {
                IsStatNull = true;
                return;
            }
            IsStatNull = false;
            LastStat = activeVehicles.Average(vehicle => (double) vehicle.Passengers);
        }
    }




}
