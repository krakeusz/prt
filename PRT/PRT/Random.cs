﻿namespace PRT
{
    public static class Random
    {
        private static readonly System.Random instance = new System.Random();

        public static System.Random Instance { get { return instance; } }
    }
}
