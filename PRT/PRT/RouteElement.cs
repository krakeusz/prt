﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public class RouteElement : InfrastructureElement
    {
        protected RouteElement(int index, Point pos, AbstractRoute route) 
            : base(Colors.Green, pos)
        {
            Index = index;
            Route = route;
        }

        public virtual RouteElement Create(int index, Point pos, AbstractRoute route)
        {
            return new RouteElement(index, pos, route);
        }

        [NonSerialized]
        protected static readonly RouteElement instance = new RouteElement(0, new Point(), null);
        public static RouteElement Instance { get { return instance; } }

        public override string Name
        {
            get { return "Route element"; }
        }

        public override string ToString()
        {
            return base.ToString() + "\n" + Route.ToString();
        }

        public override void DeleteMe(Dictionary<Point, InfrastructureElement> elements)
        {
            Route.DeleteMe(elements);
        }

        public override bool IsStationElement()
        {
            return Route.IsStationRoute();
        }

        public override AbstractStation GetStation()
        {
            return Route.GetStation();
        }

        public override List<AbstractRoute> GetAdjacentRoutes()
        {
            return new List<AbstractRoute> { Route };
        }

        public override InfrastructureElement MakeVehicleMove(Vehicle movingVehicle, ref int usedDistance)
        {
            movingVehicle.SetMaxVelocityThisTick(Route.VMax);
            int movePointsLeft = movingVehicle.MovePointsThisTick - usedDistance;
            if (movePointsLeft <= 0)
                return this;
            // index of first route element that we can't go to
            int firstBlockedPosition = Math.Min(Index + movePointsLeft + 1, Route.RouteElements.Length + 1);
            // check for vehicles
            for (int i = Index + 1; i <= Math.Min(Route.RouteElements.Length, Index + movePointsLeft + Route.MinSeparation); ++i)
            {
                if (Route.IsVehicleThere(i))
                {
                    firstBlockedPosition = Math.Max(Index + 1, i - Route.MinSeparation);
                    break;
                }
            }
            // check for blocks and junction where we must stop in front of
            for (int i = Index + 1; i < firstBlockedPosition; ++i)
            {
                if (Route.IsRouteBlocked(i))
                {
                    firstBlockedPosition = i;
                    break;
                }
            }
            usedDistance += firstBlockedPosition - 1 - Index;
            if (firstBlockedPosition == Route.RouteElements.Length + 1)
                return Route.EndNode.MakeVehicleMove(movingVehicle, ref usedDistance);
            return Route.RouteElements[firstBlockedPosition - 1];
        }

        public int Index { get; protected set; }
        public AbstractRoute Route { get; set; }
        public override Vehicle Vehicle
        {
            get { return base.Vehicle; }
            set
            {
                if (base.Vehicle == null && value != null)
                {
                    ++Route.VehicleCount;
                }
                else if (base.Vehicle != null && value == null)
                {
                    --Route.VehicleCount;
                }
                base.Vehicle = value;
            }
        }

        
    }
}
