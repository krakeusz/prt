﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms.VisualStyles;
using System.Windows.Threading;

namespace PRT
{
    public delegate double PassengerIncreaseChance(AbstractStation stationFrom, AbstractStation stationTo, int hour, int constant);

    public class Simulation
    {
        public Simulation(VisualMap visualMap, SimulationSettings settings, StatisticsGraphs statisticsGraphs)
        {
            VisualMap = visualMap;
            Map = VisualMap.Map;
            Settings = settings;
            if (settings.AutoVehicleCount)
            {
                settings.VehicleCount = Map.Elements.Sum(element => 1) / 10;
            }
            if (settings.IsTrafficLight)
            {
                foreach (var element in Map.Elements.Values)
                {
                    var node = element as AbstractNode;
                    if (node == null)
                        continue;
                    node.HasTrafficLight = true;
                    node.TrafficLightPeriod = settings.TrafficLightPeriod;
                }
            }

            Vehicles = new List<Vehicle>();
            SimulationTime = Settings.StartingTime;
            active = false;
            StatisticsGraphs = statisticsGraphs;
            RepeatableRandom = new System.Random(34723489);

            if (!Map.Depots.Any())
                throw new InvalidOperationException("No depots on map");
            if (Map.Stations.Count < 2)
                throw new InvalidOperationException("There must be at least 2 stations");

            int lastStationIndex = 0;
            try
            {
                for (lastStationIndex = 0; lastStationIndex < Map.Stations.Count; ++lastStationIndex) {
                    Map.Stations[lastStationIndex].InitSimulation(Map.Stations, Map.Depots, Settings);
                }
            }
            catch(Exception e)
            {
                for (int i = 0; i < lastStationIndex; ++i)
                    Map.Stations[i].ClearSimulation();
                throw new InvalidOperationException(e.Message);
            }

            // nothrow from now on

            for (int i = 0; i < Settings.VehicleCount; ++i)
            {
                var newVehicle = new Vehicle(Map.Depots[i % Map.Depots.Count], Settings.Pathfinder,
                                             Settings.VehicleCapacity, Settings.VehicleVMax, Settings.VehicleA);
                Vehicles.Add(newVehicle);
            }

            Timer = new DispatcherTimer();
            UpdateInterval();
            Timer.Tick += TimerOnTick;

            Active = true;
        }

        public void UpdateInterval()
        {
            Timer.Interval = new TimeSpan(0, 0, 0, 0, Settings.IntervalBetweenTicks);
        }

        public void NextTick()
        {
            if (!Active)
                TimerOnTick(this, null);
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            SimulationTime = SimulationTime.AddMinutes(1);
            // create new passengers on stations
            foreach (var station in Map.Stations)
            {
                station.CreatePassengers(Settings.ChanceDelegate, SimulationTime.Hour, Settings.PassengerIncreaseConstant, RepeatableRandom);
                station.AssignNewPassengers();
                station.CallForVehicles(Settings);
                station.SendVehiclesToDepot(Settings);
                station.LoadPassengers(Settings);
                station.UnloadPassengers(Settings);
            }
            // get a vehicle out of each depot, if possible and needed
            foreach(var depot in Map.Depots)
            {
                depot.StartVehicle();
            }
            // remember the move that each vehicle will make
            var vehicleMoves = new List<Tuple<Vehicle, InfrastructureElement>>();
            foreach(var vehicle in Vehicles)
            {
                if (vehicle.Active && !vehicle.Broken)
                    vehicleMoves.Add(new Tuple<Vehicle, InfrastructureElement>(vehicle, vehicle.MakeMove()));
            }
            var d = new Dictionary<Point, Vehicle>();
            // make the moves
            foreach (var vehicleMove in vehicleMoves)
            {
                if (vehicleMove.Item2 != null)
                    vehicleMove.Item1.Location = vehicleMove.Item2;
            }
            // stop vehicles in depots, if needed
            foreach (var depot in Map.Depots) {
                depot.StopVehicle();
            }
            // cleanup
            foreach (var element in Map.Elements.Values)
            {
                element.ClearAfterTick();
            }
            foreach(var vehicle in Vehicles)
            {
                vehicle.ClearAfterTick();
            }
            Settings.Info.Content = SimulationTime.ToShortTimeString() + " | passengers waiting: " +
                                    Map.Stations.Sum(station => station.NumberOfPassengersWaiting()) +
                                    " | passengers on board: " + Vehicles.Sum(vehicle => vehicle.Passengers) +
                                    " | LMB: break element   MMB: move map   Wheel: zoom";
            if (StatisticsGraphs != null)
                StatisticsGraphs.DrawNewPoints();
            VisualMap.DrawMap();
            if (MainWindow != null)
                MainWindow.OnAfterTick();
        }

        public void DeleteMe()
        {
            Active = false;
            foreach (var vehicle in Vehicles)
            {
                vehicle.DeleteMe();
            }
            Vehicles.Clear();
            foreach (var element in Map.Elements.Values)
            {
                element.ClearSimulation();
            }
            foreach (var station in Map.Stations)
            {
                station.ClearSimulation();
            }
            VisualMap.DrawMap();
        }

        public void OnMouseLeftButtonUp(Point p)
        {
            bool changed = false;
            InfrastructureElement clickedElement;
            switch (DrawSettings.Instance.State)
            {
                case DrawSettings.EState.Pointer:
                    var clickedVehicles = Vehicles.Where(vehicle => !vehicle.IsInDepot() && vehicle.Location.Pos == p).ToList();
                    if (clickedVehicles.Any())
                    {
                        clickedVehicles.First().Broken = true;
                        clickedVehicles.First().TicksToRepair = SimulationSettings.BrokenPeriod;
                        changed = true;
                    }
                    else if (Map.Elements.TryGetValue(p, out clickedElement))
                    {
                        clickedElement.Broken = true;
                        clickedElement.TicksToRepair = SimulationSettings.BrokenPeriod;
                        changed = true;
                    }
                    break;
            }
            if (changed)
                VisualMap.DrawMap();
        }

        public VisualMap VisualMap { get; private set; }
        public Map Map { get; set; }
        public SimulationSettings Settings { get; private set; }
        public List<Vehicle> Vehicles { get; private set; }
        public DispatcherTimer Timer { get; set; }
        public DateTime SimulationTime { get; set; }
        public StatisticsGraphs StatisticsGraphs { get; set; }
        public MainWindow MainWindow { get; set; }
        // for creating passengers, the result should be the same for every simulation restart
        public System.Random RepeatableRandom { get; set; }

        private bool active;
        public bool Active 
        { 
            get { return active; } 
            set
            {
                if (active == false && value)
                {
                    active = true;
                    Timer.Start();
                }
                else if (active && value == false)
                {
                    active = false;
                    Timer.Stop();
                }
            }
        }
    }

    public class SimulationSettings
    {
        public SimulationSettings(StatusBarItem info)
        {
            AutoVehicleCount = true;
            VehicleCount = 40;
            VehicleCapacity = 4;
            VehicleVMax = 4;
            VehicleA = 1;
            VehicleFillPercentage = 50;
            Pathfinder = new StaticDijkstraPathfinder(null);
            ChanceDelegate = RushIncrease;
            StartingTime = new DateTime(2000, 1, 1, 6, 0, 0);
            PassengerIncreaseConstant = 10;
            Info = info;
            IntervalBetweenTicks = 100;
            ChanceToSendEmptyVehicleToDepot = 1;
            ChanceToCallForVehicle = 0.05;
            IsTrafficLight = false;
            BrokenPeriod = 30;
        }

        public double LinearIncrease(AbstractStation stationFrom, AbstractStation stationTo, int hour, int constant)
        {
            return stationFrom == stationTo ? 0 : constant/(double) 60;
        }

        public double RushIncrease(AbstractStation stationFrom, AbstractStation stationTo, int hour, int constant)
        {
            int[] results =  {2, 1, 1, 0, 1, 2, 3, 6, 8, 6, 5, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 3, 3, 3};
            return stationFrom == stationTo ? 0 : results[hour] * constant / ((double) 60*5);
        }

        public bool AutoVehicleCount { get; set; }
        public int VehicleCount { get; set; }
        public Pathfinder Pathfinder { get; set; }
        public int VehicleCapacity { get; set; }
        // how many passengers should be in the vehicle to start it
        public int VehicleFillPercentage { get; set; }
        public int VehicleVMax { get; set; }
        public int VehicleA { get; set; }
        public DateTime StartingTime { get; set; }
        public PassengerIncreaseChance ChanceDelegate { get; set; }
        public int PassengerIncreaseConstant { get; set; }
        public StatusBarItem Info { get; private set; }
        public int IntervalBetweenTicks { get; set; }
        // probability that... you know. Every tick it gets the same chance to be sent
        public double ChanceToSendEmptyVehicleToDepot { get; set; }
        // probability that if VehicleCapacity passengers wait at the station, we will call for a vehicle
        // after calculations, will be not larger than 20%
        public double ChanceToCallForVehicle { get; set; }
        public bool IsTrafficLight { get; set; }
        public int TrafficLightPeriod { get; set; }
        // ticks before broken element/vehicle gets repaired
        public static int BrokenPeriod { get; set; }
    }
}
