﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;

namespace PRT
{
    [Serializable]
    public sealed class Map
    {
        public Map()
        {
            Stations = new List<AbstractStation>();
            Depots = new List<Depot>();
            Elements = new Dictionary<Point, InfrastructureElement>();
        }

        public void OnMouseLeftButtonUp(Point p)
        {
            bool changed = false;
            try {
                InfrastructureElement clickedElement;
                switch (DrawSettings.Instance.State) {
                    case DrawSettings.EState.Node:
                        // create new node, if possible
                        new Node(p, Elements);
                        changed = true;
                        break;
                    case DrawSettings.EState.RouteStart:
                        // mark this node as a new route start
                        if (Elements.TryGetValue(p, out clickedElement)) {
                            var clickedNode = clickedElement as AbstractNode;
                            if (clickedNode == null || !clickedNode.IsUserLinkable())
                                break;
                            DrawSettings.Instance.HighlightedRouteStart = clickedNode;
                            DrawSettings.Instance.State = DrawSettings.EState.RouteEnd;
                            changed = true;
                        }
                        break;
                    case DrawSettings.EState.RouteEnd:
                    case DrawSettings.EState.AutoRouteEnd:
                        // create a route, but if there is no end node, create it also if possible

                        // true if there is no node where we clicked
                        bool makeNode = false;
                        // clicked on a free space
                        if (Elements.TryGetValue(p, out clickedElement) == false) {
                            makeNode = true;
                        }
                            // clicked on a route
                        else {
                            var clickedRouteElement = clickedElement as RouteElement;
                            if (clickedRouteElement != null) {
                                makeNode = true;
                            }
                        }
                        if (makeNode) {
                            Point[] unused;
                            if (!AbstractRoute.IsValidRoute(DrawSettings.Instance.HighlightedRouteStart.Pos, p, out unused, Elements))
                                throw new Exception("Invalid route.");
                            clickedElement = new Node(p, Elements);
                        }
                        // if we didn't click on a free space nor a route, then we clicked on a node 
                    {
                            var clickedNode = clickedElement as AbstractNode;
                            Debug.Assert(clickedNode != null);
                            if (!clickedNode.IsUserLinkable())
                                break;
                            new Route(DrawSettings.Instance.HighlightedRouteStart, clickedNode, Elements);
                            DrawSettings.Instance.HighlightedRouteStart = null;
                            switch (DrawSettings.Instance.State) {
                                case DrawSettings.EState.RouteEnd:
                                    DrawSettings.Instance.State = DrawSettings.EState.RouteStart;
                                    break;
                                case DrawSettings.EState.AutoRouteEnd:
                                    DrawSettings.Instance.State = DrawSettings.EState.Auto;
                                    break;
                            }
                        }
                    changed = true;
                    break;
                    case DrawSettings.EState.Depot:
                    // create new route, if possible
                    Depots.Add(new Depot(p, Elements));
                    changed = true;
                    break;
                    case DrawSettings.EState.Station:
                    // create new station, if possible
                    switch (DrawSettings.Instance.StationType) {
                        case DrawSettings.EStationType.Fifo:
                            Stations.Add(new FifoStation(DrawSettings.Instance.StationSize, DrawSettings.Instance.StationDirection, p, Elements));
                            changed = true;
                            break;
                        case DrawSettings.EStationType.Terminus:
                            Stations.Add(new TerminusStation(DrawSettings.Instance.StationSize, DrawSettings.Instance.StationDirection, p, Elements));
                            changed = true;
                            break;
                    }
                    break;
                    case DrawSettings.EState.Auto:
                    // node -> create route
                    // space -> create node and switch to route creating
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        var clickedNode = clickedElement as AbstractNode;
                        if (clickedNode != null) {
                            if (!clickedNode.IsUserLinkable())
                                break;
                            DrawSettings.Instance.HighlightedRouteStart = clickedNode;
                            DrawSettings.Instance.State = DrawSettings.EState.AutoRouteEnd;
                            changed = true;
                            break;
                        }
                    }
                    DrawSettings.Instance.HighlightedRouteStart = new Node(p, Elements);
                    DrawSettings.Instance.State = DrawSettings.EState.AutoRouteEnd;
                    changed = true;
                    break;
                    case DrawSettings.EState.Info:
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        MessageBox.Show(clickedElement.ToString(), "PRT info",
                                        MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    break;
                }
            } catch (Exception e) {
                MessageBox.Show("Cannot do that: " + e.Message);
            }

            if (changed)
                VisualMap.DrawMap();
        }

        public void OnMouseRightButtonUp(Point p)
        {
            InfrastructureElement clickedElement;
            bool changed = false;
            switch (DrawSettings.Instance.State) {
                case DrawSettings.EState.Node:
                    // delete node
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        var clickedNode = clickedElement as Node;
                        if (clickedNode == null || clickedNode.IsStationElement())
                            break;
                        clickedNode.DeleteMe(Elements);
                        changed = true;
                    }
                    break;
                case DrawSettings.EState.RouteStart:
                    // delete route
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        var clickedRouteElement = clickedElement as RouteElement;
                        if (clickedRouteElement == null)
                            break;
                        if (clickedRouteElement.IsStationElement())
                            break;
                        clickedRouteElement.DeleteMe(Elements);
                        changed = true;
                    }
                    break;
                case DrawSettings.EState.RouteEnd:
                case DrawSettings.EState.AutoRouteEnd:
                    // cancel creating new route
                    DrawSettings.Instance.HighlightedRouteStart = null;
                    switch (DrawSettings.Instance.State) {
                        case DrawSettings.EState.RouteEnd:
                            DrawSettings.Instance.State = DrawSettings.EState.RouteStart;
                            break;
                        case DrawSettings.EState.AutoRouteEnd:
                            DrawSettings.Instance.State = DrawSettings.EState.Auto;
                            break;
                    }
                    changed = true;
                    break;
                case DrawSettings.EState.Depot:
                    // delete depot
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        var clickedDepot = clickedElement as Depot;
                        if (clickedDepot == null)
                            break;
                        Depots.Remove(clickedDepot);
                        clickedDepot.DeleteMe(Elements);
                        changed = true;
                    }
                    break;
                case DrawSettings.EState.Station:
                    // delete station if station route/node was clicked
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        if (!clickedElement.IsStationElement())
                            break;
                        Stations.Remove(clickedElement.GetStation());
                        clickedElement.DeleteMe(Elements);
                        changed = true;
                    }
                    break;
                case DrawSettings.EState.Auto:
                    // delete anything
                    if (Elements.TryGetValue(p, out clickedElement)) {
                        if (clickedElement.IsStationElement())
                            Stations.Remove(clickedElement.GetStation());
                        var clickedDepot = clickedElement as Depot;
                        if (clickedDepot != null)
                            Depots.Remove(clickedDepot);
                        clickedElement.DeleteMe(Elements);
                        changed = true;
                    }
                    break;
            }

            if (changed)
                VisualMap.DrawMap();
        }

        public void OnMouseMiddleButtonUp(Point p)
        {
            bool changed = false;
            switch (DrawSettings.Instance.State) {
                case DrawSettings.EState.Station:
                    DrawSettings.Instance.StationDirection.RotateRight();
                    DrawSettings.Instance.SetStationInfo();
                    break;
            }

            if (changed)
                VisualMap.DrawMap();
        }

        public List<AbstractStation> Stations { get; private set; }
        public List<Depot> Depots { get; private set; }

        // everything visible as a square on a map
        public Dictionary<Point, InfrastructureElement> Elements { get; private set; }

        [NonSerialized] private VisualMap _visualMap;
        public VisualMap VisualMap { get { return _visualMap; } set { _visualMap = value; } }
    }
}
