﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;

namespace PRT
{
    public sealed class DrawSettings
    {
        private DrawSettings()
        {
            _state = EState.Pointer;
            RouteVMax = 1000000;
            RouteMinSeparation = 0;
            highlightedRouteStart = null; // do not invoke set
        }

        static readonly DrawSettings _instance = new DrawSettings();

        // remember to set Map and Info
        public static DrawSettings Instance { get { return _instance; } }

        public enum EState
        {
            Pointer,
            Node,
            RouteStart,
            RouteEnd,
            Depot,
            Station,
            Auto,
            AutoRouteEnd,
            Info
        }

        public enum EStationType
        {
            Fifo,
            Terminus
        }

        private void RemoveNewRoute()
        {
            if (HighlightedRouteStart == null) return;
            HighlightedRouteStart = null;
            VisualMap.DrawMap();
        }

        public void SetStationInfo()
        {
            Info.Content = "LMB on large space: create station (" + StationDirection.ToString()
                + ")   MMB: rotate clockwise   RMB on station element: delete station";
        }

        public void UpdateInfo()
        {
            switch (State) {
                case EState.Pointer:
                    Info.Content = "";
                    break;
                case EState.Node:
                    Info.Content = "LMB on space: create junction   RMB on junction: delete junction";
                    break;
                case EState.RouteStart:
                    Info.Content = "LMB on node: route begin   RMB on route: delete route";
                    break;
                case EState.RouteEnd:
                    Info.Content = "LMB on node: route end   RMB: cancel";
                    break;
                case EState.Depot:
                    Info.Content = "LMB on space: create depot   RMB on depot: delete depot";
                    break;
                case EState.Station:
                    SetStationInfo();
                    break;
                case EState.Auto:
                    Info.Content = "LMB on space: junction; on node: route;   RMB: delete";
                    break;
                case EState.AutoRouteEnd:
                    Info.Content = "LMB on node: route end   RMB: cancel";
                    break;
                case EState.Info:
                    Info.Content = "LMB on anything: information";
                    break;
            }
            if (IsRouteInLine && (State == EState.AutoRouteEnd || State == EState.RouteEnd))
                Info.Content += "    <in line>";
        }

        private EState _state;
        public EState State { 
            get
            {
                return _state;
            }
            set
            {
                if ((_state == EState.RouteEnd || _state == EState.AutoRouteEnd)
                    && value != EState.RouteEnd && value != EState.AutoRouteEnd)
                {
                    RemoveNewRoute();
                }
                _state = value;
                if (_state != EState.AutoRouteEnd && _state != EState.RouteEnd)
                    IsRouteInLine = false;
                UpdateInfo();   
            } 
        }

        public Direction StationDirection { get; set; }
        public int StationSize { get; set; }
        public EStationType StationType { get; set; }
        public int RouteVMax { get; set; }
        public int RouteMinSeparation { get; set; }

        private bool isRouteInLine;
        public bool IsRouteInLine
        {
            get { return isRouteInLine; }
            set 
            {
                isRouteInLine = value;
                UpdateInfo();
            }
        }

        private AbstractNode highlightedRouteStart;
        public AbstractNode HighlightedRouteStart
        {
            get { return highlightedRouteStart; }
            set
            {
                if (value == null)
                {
                    highlightedRouteStart.Color = highlightedRouteStart.DefaultColor;
                }
                else
                {
                    value.Color = value.HighlightedColor;
                }
                highlightedRouteStart = value;
            }
        }
        public StatusBarItem Info { get; set; }
        public VisualMap VisualMap { get; set; }
    }
}
