﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class FifoPassengerNode : PassengerNode
    {
        public FifoPassengerNode(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {}

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            // check if there is free place in front of us, and try to get there instead
            bool foundUs = false;
            PassengerNode currentDestination = this;
            Debug.Assert(this == currentDestination);
            foreach (var otherPassengerNode in Station.PassengerNodes)
            {
                if (!foundUs)
                {
                    if (otherPassengerNode == this)
                        foundUs = true;
                }
                else
                {
                    if (otherPassengerNode.Vehicle == null && !otherPassengerNode.Reserved) {
                        currentDestination.Reserved = false;
                        otherPassengerNode.Reserved = true;
                        movingVehicle.Destination = currentDestination = otherPassengerNode;
                    } else
                        break;
                }
            }
            if (currentDestination != this)
                return; // switched to another node
            // so, finally there
            DockVehicle(movingVehicle);
        }

        public override void StartVehicle()
        {
            DockedVehicle.Destination = Station.Exit;
            DockedVehicle.LastPassengerNode = this;
            base.StartVehicle();
        }
    }
}
