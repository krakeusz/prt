﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PRT
{
    /// <summary>
    /// Interaction logic for StatsWindow.xaml
    /// </summary>
    public partial class StatsWindow : Window
    {
        public StatsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Width = Screen.PrimaryScreen.WorkingArea.Width * 2 / 3;
            Height = Screen.PrimaryScreen.WorkingArea.Height;
            Left = (Screen.PrimaryScreen.WorkingArea.Width - Width)/2;
            Top = 0;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsRealClose)
            {
                Hide();
                e.Cancel = true;
            }
        }

        public bool IsRealClose { get; set; }
    }
}
