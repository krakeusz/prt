﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class StationRoute : AbstractRoute
    {
        public StationRoute(AbstractStation station, AbstractNode begin, AbstractNode end,
                            Dictionary<Point, InfrastructureElement> elements, AbstractRoute patternRoute = null)
            : base(begin, end, elements, RouteElement.Instance, patternRoute)
        {
            Station = station;
        }

        protected StationRoute(AbstractStation station, AbstractNode begin, AbstractNode end,
                               Dictionary<Point, InfrastructureElement> elements, RouteElement patternElement,
                               AbstractRoute patternRoute)
            : base(begin, end, elements, patternElement, patternRoute)
        {
            Station = station;
        }

        public override void DeleteMe(Dictionary<Point,InfrastructureElement> elements)
        {
            Station.DeleteMe(elements);
        }

        public override bool IsStationRoute()
        {
            return true;
        }

        public void DeleteOnlyMe(Dictionary<Point, InfrastructureElement> elements)
        {
            base.DeleteMe(elements);
        }

        public override AbstractStation GetStation()
        {
            return Station;
        }

        public AbstractStation Station { get; protected set; }
        
    }
}
