﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;

namespace PRT
{
    [Serializable]
    public class FifoStation : AbstractStation
    {
        public FifoStation(int size, Direction direction, Point pos, Dictionary<Point, InfrastructureElement> elements)
            : base(size)
        {
            var turtle = new Turtle(direction, pos);
            Debug.Assert(size >= 1);
            try
            {
                var begin = new ExternalStationNode(turtle.Position, this, elements);
                ExternalStationNodes.Add(begin);

                turtle.MoveForward(2);
                Entrance = new StationEntrance(turtle.Position, this, elements);
                StationRoutes.Add(new StationRoute(this, begin, Entrance, elements));

                turtle.RotateLeft();
                StationNode lastNode = Entrance;
                for(int i = 0; i < Size; ++i)
                {
                    turtle.MoveForward(1);
                    var thisNode = new FifoPassengerNode(turtle.Position, this, elements);
                    PassengerNodes.Add(thisNode);
                    StationRoutes.Add(new StationRoute(this, lastNode, thisNode, elements));
                    lastNode = thisNode;
                }
                turtle.MoveForward(1);
                Exit = new FifoStationExit(turtle.Position, this, elements);
                StationRoutes.Add(new StationRoute(this, lastNode, Exit, elements));

                turtle.RotateLeft();
                turtle.MoveForward(2);
                var end = new ExternalStationNode(turtle.Position, this, elements);
                ExternalStationNodes.Add(end);
                StationRoutes.Add(new StationRoute(this, Exit, end, elements));
            }
            catch (Exception e)
            {
                DeleteMe(elements);
                throw new Exception("Cannot build station: " + e.Message);
            }
        }

        public override PassengerNode FindFreePassengerNode()
        {
            // find first occupied node
            int firstReserved = PassengerNodes.Count;
            for (int i = 0; i < PassengerNodes.Count; ++i)
            {
                if (PassengerNodes[i].Reserved)
                {
                    firstReserved = i;
                    break;
                }
            }
            if (firstReserved == 0)
                return null; // no space, first node is reserved
            PassengerNodes[firstReserved - 1].Reserved = true;
            return PassengerNodes[firstReserved - 1];
        }
    }
}
