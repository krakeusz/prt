﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PRT
{
    class Turtle
    {
        public Turtle(Direction d, Point pos)
        {
            Direction = d.Clone();
            Position = pos.Clone();
        }

        public void MoveForward(int step)
        {
            switch(Direction.Dir)
            {
                case Direction.N:
                    Position.Y -= step;
                    break;
                case Direction.S:
                    Position.Y += step;
                    break;
                case Direction.W:
                    Position.X -= step;
                    break;
                case Direction.E:
                    Position.X += step;
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        public void RotateLeft()
        {
            Direction.RotateLeft();
        }

        public void RotateRight()
        {
            Direction.RotateRight();
        }

        public Point Position { get; set; }
        public Direction Direction { get; set; }
    }
}
