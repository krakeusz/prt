﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public class StationEntrance : InternalStationNode
    {
        public StationEntrance(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
        }

        public override bool IsPathfinderCrossable()
        {
            return false;
        }

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            var freeNode = Station.FindFreePassengerNode();
            if (freeNode == null)
                return; // do nothing and try again next time
            movingVehicle.Destination = freeNode;
        }
    }
}
