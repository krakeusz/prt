﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public class StationExit : InternalStationNode
    {
        public StationExit(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
        }

        public override bool IsPathfinderCrossable()
        {
            return false;
        }

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            if (movingVehicle.DepotDestination != null)
            {
                movingVehicle.Destination = movingVehicle.DepotDestination;
            }
            else
            {
                movingVehicle.Destination = movingVehicle.PassengersDestination.Entrance;
            }
        }
    }
}
