﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;
using ZedGraph;

namespace PRT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Info = new StatusBarItem();
            
            _visualMap = new VisualMap(Info);
            Simulation = null;
            SimulationSettings = null;
            StatsWindow = new StatsWindow();

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MinWidth = tabControl.ActualWidth + mapCanvas.MinWidth + 50;
            MinHeight = mapCanvas.MinHeight;

            DefaultButtonBackground = btnPointer.Background;
            SelectedButtonBackground = Brushes.BurlyWood;
            RadioButtons = new Button[] { btnPointer, btnNewNode, btnNewRoute, btnNewDepot, btnNewStation, btnAuto, btnInfo };

            statusBar.Items.Add(Info);
            mapCanvas.MouseUp += _visualMap.OnMouseUp;
            mapCanvas.MouseMove += _visualMap.OnMouseMove;
            mapCanvas.MouseDown += _visualMap.OnMouseDown;
            mapCanvas.MouseWheel += _visualMap.OnMouseWheel;
            mapCanvas.Children.Add(_visualMap);
            SelectButton(btnPointer);
            SimulationSettings = new SimulationSettings(Info);
            refreshSimulationButtons();
            StatisticsGraphs = new StatisticsGraphs(StatsWindow.hosStats, StatsWindow.txtShortStats);
        }

        private VisualMap _visualMap;

        private void btnPointer_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnPointer);
            DrawSettings.Instance.State = DrawSettings.EState.Pointer;
        }

        private void SelectButton(Button b)
        {
            ResetButtonBackgrounds(b);
            b.Background = SelectedButtonBackground;
        }

        private void ResetButtonBackgrounds(Button except)
        {
            foreach (var b in RadioButtons.Where(b => b != except))
                b.Background = DefaultButtonBackground;
        }

        private Brush DefaultButtonBackground { get; set; }
        private Brush SelectedButtonBackground { get; set; }

        private Button[] RadioButtons { get; set; }

        private StatusBarItem Info { get; set; }

        private void btnNewNode_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnNewNode);
            DrawSettings.Instance.State = DrawSettings.EState.Node;
        }

        private void btnNewRoute_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnNewRoute);
            DrawSettings.Instance.State = DrawSettings.EState.RouteStart;
        }

        private void btnNewDepot_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnNewDepot);
            DrawSettings.Instance.State = DrawSettings.EState.Depot;
        }

        private void radFifo_Checked(object sender, RoutedEventArgs e)
        {
            DrawSettings.Instance.StationType = DrawSettings.EStationType.Fifo;
        }

        private void radTerminus_Checked(object sender, RoutedEventArgs e)
        {
            DrawSettings.Instance.StationType = DrawSettings.EStationType.Terminus;
        }

        private void btnNewStation_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnNewStation);
            DrawSettings.Instance.StationDirection = new Direction(Direction.N);
            DrawSettings.Instance.State = DrawSettings.EState.Station;
        }

        private void btnAuto_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnAuto);
            DrawSettings.Instance.State = DrawSettings.EState.Auto;
        }

        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            SelectButton(btnInfo);
            DrawSettings.Instance.State = DrawSettings.EState.Info;
        }

        private void numStationSize_ValueChanged(object sender, EventArgs e)
        {
            DrawSettings.Instance.StationSize = (int)numStationSize.Value;
        }

        private void numRouteVmax_ValueChanged(object sender, EventArgs e)
        {
            DrawSettings.Instance.RouteVMax = (int) numRouteVmax.Value;
        }

        private void numSeparation_ValueChanged(object sender, EventArgs e)
        {
            DrawSettings.Instance.RouteMinSeparation = (int) numSeparation.Value;
        }

        private void refreshSimulationButtons()
        {
            if (Simulation == null)
            {
                btnStartPauseSimulation.Content = "Start";
                btnStopSimulation.IsEnabled = false;
                btnOneStep.IsEnabled = false;
                tabEdit.IsEnabled = true;
                grpSettings.IsEnabled = true;
            }
            else
            {
                if (Simulation.Active)
                {
                    btnStartPauseSimulation.Content = "Pause";
                    btnOneStep.IsEnabled = false;
                }
                else
                {
                    btnStartPauseSimulation.Content = "Resume";
                    btnOneStep.IsEnabled = true;
                }
                btnStopSimulation.IsEnabled = true;
                tabEdit.IsEnabled = false;
                grpSettings.IsEnabled = false;
            }
        }

        private Simulation Simulation { get; set; }
        private SimulationSettings SimulationSettings { get; set; }
        private StatsWindow StatsWindow { get; set; }
        private StatisticsGraphs StatisticsGraphs { get; set; }

        private void tabSimulation_GotFocus(object sender, RoutedEventArgs e)
        {
            btnPointer_Click(tabSimulation, null);
        }

        private void btnStartPauseSimulation_Click(object sender, RoutedEventArgs e)
        {
            if (Simulation == null)
            {
                try
                {
                    Simulation = new Simulation(_visualMap, SimulationSettings, StatisticsGraphs);
                    Simulation.MainWindow = this;
                    StatisticsGraphs.ClearSimulation();
                    StatisticsGraphs.AddSimulation(Simulation);
                    _visualMap.Simulation = Simulation;
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Cannot start simulation: " + exception.Message);
                    return;
                }
            }
            else
            {
                Simulation.Active = !Simulation.Active;
            }
            refreshSimulationButtons();
        }

        private void btnStopSimulation_Click(object sender, RoutedEventArgs e)
        {
            _visualMap.Simulation = null;
            Simulation.DeleteMe();
            Simulation = null;
            refreshSimulationButtons();
        }

        private void btnOneStep_Click(object sender, RoutedEventArgs e)
        {
            Debug.Assert(Simulation != null && Simulation.Active == false);
            Simulation.NextTick();
        }

        private void numVehicles_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null) {
                SimulationSettings.VehicleCount = (int)numVehicles.Value;
            }
        }

        private void radVehicleAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                numVehicles.Enabled = false;
                SimulationSettings.AutoVehicleCount = true;
            }
       }

        private void radVehicleManual_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                numVehicles.Enabled = true;
                SimulationSettings.AutoVehicleCount = false;
                SimulationSettings.VehicleCount = (int) numVehicles.Value;
            }
        }

        private void numVMax_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.VehicleVMax = (int) numVMax.Value;
            }
        }

        private void numCapacity_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.VehicleCapacity = (int) numCapacity.Value;
            }
        }

        private void numA_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.VehicleA = (int) numA.Value;
            }
        }

        private void numFillPercent_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.VehicleFillPercentage = (int) numFillPercent.Value;
            }
        }

        private void numPassengerFactor_ValueChanged(object sender, EventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.PassengerIncreaseConstant = (int) numPassengerFactor.Value;
            }
        }

        private void radPassengerConstant_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.ChanceDelegate = SimulationSettings.LinearIncrease;
            }
        }

        private void radPassengerRush_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.ChanceDelegate = SimulationSettings.RushIncrease;
            }
        }

        private void radTrafficLightsOff_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.IsTrafficLight = false;
            }
        }

        private void radTrafficLightsOn_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
            {
                SimulationSettings.IsTrafficLight = true;
                SimulationSettings.TrafficLightPeriod = (int) numTrafficLightsPeriod.Value;
            }
        }

        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (SimulationSettings != null)
            {
                double x = sliSpeed.Value;
                double y = 20.0/49.0*x*x - 20.0/49.0*x + 1;
                SimulationSettings.IntervalBetweenTicks = (int) y;
                if (Simulation != null)
                    Simulation.UpdateInterval();
            }
        }

        private void radStaticDijkstra_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
                SimulationSettings.Pathfinder = new StaticDijkstraPathfinder(null);
        }

        private void radDynamicDijkstra_Checked(object sender, RoutedEventArgs e)
        {
            if (SimulationSettings != null)
                SimulationSettings.Pathfinder = new DynamicDijkstraPathfinder(null);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = "PRT map files (*.prt)|*.prt";
            saveDialog.InitialDirectory = Directory.GetCurrentDirectory();
            if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Stream saveStream;
                if ((saveStream = saveDialog.OpenFile()) != null)
                {
                    try
                    {
                        btnPointer_Click(this, null);
                        var bf = new BinaryFormatter();
                        bf.Serialize(saveStream, _visualMap.Map);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "PRT", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    saveStream.Close();
                }
            }
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var loadDialog = new OpenFileDialog();
            loadDialog.Filter = "PRT map files (*.prt)|*.prt";
            loadDialog.InitialDirectory = Directory.GetCurrentDirectory();
            if (loadDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                Stream loadStream;
                if ((loadStream = loadDialog.OpenFile()) != null) {
                    try {
                        btnPointer_Click(this, null);
                        var bf = new BinaryFormatter();
                        _visualMap.Map = (Map)bf.Deserialize(loadStream);
                        _visualMap.ClearTransforms();
                    } catch (Exception ex) {
                        MessageBox.Show(ex.Message, "PRT", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    loadStream.Close();
                }
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            if (_visualMap.Map.Elements.Any())
            {
                if (MessageBox.Show("Are you sure you want to clear the whole map?", "PRT",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
            }
            _visualMap.Map = new Map();
            _visualMap.ClearTransforms();
        }

        private void btnStats_Click(object sender, RoutedEventArgs e)
        {
            StatsWindow.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StatsWindow.IsRealClose = true;
            StatsWindow.Close();
        }

        public void OnAfterTick()
        {
            if (chkPauseAfter.IsChecked == true &&
                Simulation.SimulationTime >=
                Simulation.Settings.StartingTime + new TimeSpan(0, (int) numStopAfter.Value, 0, 0))
            {
                if (Simulation.Active)
                    btnStartPauseSimulation_Click(this, null);
            }
        }
    }
}