﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public class Depot : AbstractNode
    {
        public Depot(Point pos, Dictionary<Point, InfrastructureElement> elements)
            : base(Colors.Sienna, Colors.SaddleBrown, pos, false, elements)
        {
            StoppedVehicles = new Queue<Vehicle>();
            WaitingVehicles = new Queue<Vehicle>();
        }

        [OnDeserializing]
        private void DefaultQueues(StreamingContext context)
        {
            StoppedVehicles = new Queue<Vehicle>();
            WaitingVehicles = new Queue<Vehicle>();
        }

        public override string Name
        {
            get { return "Depot"; }
        }

        public override bool IsDepot()
        {
            return true;
        }

        public override bool IsPathfinderCrossable()
        {
            return false;
        }

        public override void ClearSimulation()
        {
            base.ClearSimulation();
            StoppedVehicles.Clear();
            WaitingVehicles.Clear();
        }

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            movingVehicle.Destination = null; // make sure it will stop
            movingVehicle.DepotDestination = null;
            movingVehicle.Active = false;
        }

        // hides vehicle
        public void StopVehicle()
        {
            if (Vehicle != null && Vehicle.Destination == null)
            {
                Debug.Assert(!Vehicle.Active);
                StoppedVehicles.Enqueue(Vehicle);
                Vehicle = null;
            }
        }

        // request sent from station when it wants a vehicle
        // returns true if there is a free vehicle
        public bool RequestVehicle(AbstractStation station)
        {
            if (!StoppedVehicles.Any())
                return false;
            var selectedVehicle = StoppedVehicles.Dequeue();
            selectedVehicle.Destination = station.Entrance;
            selectedVehicle.CalledToStation = station;
            WaitingVehicles.Enqueue(selectedVehicle);
            return true;
        }

        // starts a waiting vehicle, if there is any
        public void StartVehicle()
        {
            if (Vehicle != null || !WaitingVehicles.Any())
                return;
            Vehicle = WaitingVehicles.Dequeue();
            Vehicle.Active = true;
        }

        // vehicles stopped at the depot with no orders
        [NonSerialized]
        private Queue<Vehicle> _stoppedVehicles;
        public Queue<Vehicle> StoppedVehicles
        {
            get { return _stoppedVehicles; }
            set { _stoppedVehicles = value; }
        }

        // vehicles stopped at the depot with orders, waiting for a free track
        [NonSerialized]
        private Queue<Vehicle> _waitingVehicles;
        public Queue<Vehicle> WaitingVehicles
        {
            get { return _waitingVehicles; }
            set { _waitingVehicles = value; }
        }
    }
}
