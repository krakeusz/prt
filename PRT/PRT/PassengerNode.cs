﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public abstract class PassengerNode : InternalStationNode
    {
        protected PassengerNode(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
            DefaultColor = Color = Colors.Yellow;
            HighlightedColor = Colors.Goldenrod;
            Reserved = false;
            DockedVehicle = null;
        }

        public override string Name
        {
            get
            {
                return "Passenger node";
            }
        }

        public override void ClearSimulation()
        {
            base.ClearSimulation();
            Reserved = false;
            DockedVehicle = null;
        }

        public void DockVehicle(Vehicle movingVehicle)
        {
            movingVehicle.IsDocked = true;
            movingVehicle.Active = false;
            movingVehicle.Destination = null;
            if (movingVehicle.CalledToStation == Station)
                --Station.VehiclesCalledFromDepot;
            movingVehicle.CalledToStation = null;
            DockedVehicle = movingVehicle;
        }

        // Destination and LastPassengerNode are to be set in derived methods
        public virtual void StartVehicle()
        {
            DockedVehicle.IsDocked = false;
            DockedVehicle.Active = true;
            DockedVehicle = null;
        }

        [NonSerialized]
        private bool _reserved;
        public bool Reserved
        {
            get { return _reserved; }
            set { _reserved = value; }
        }

        [NonSerialized]
        private Vehicle _dockedVehicle;
        public Vehicle DockedVehicle
        {
            get { return _dockedVehicle; }
            set { _dockedVehicle = value; }
        }
    }
}
