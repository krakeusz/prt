﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class TerminusPassengerNode : PassengerNode
    {
        public TerminusPassengerNode(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {}

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            DockVehicle(movingVehicle);
        }

        public override void StartVehicle()
        {
            Debug.Assert(Station.PassengerNodes.Count == Station.InternalStationNodes.Count);
            // send the vehicle to corresponding internal node hub, where it will free the platform
            for (int i = 0; i < Station.PassengerNodes.Count; ++i)
            {
                if (Station.PassengerNodes[i] == this)
                {
                    DockedVehicle.Destination = Station.InternalStationNodes[i];
                    DockedVehicle.LastPassengerNode = this;
                    base.StartVehicle();
                    return;
                }
            }
            Debug.Assert(false);
        }
    }
}
