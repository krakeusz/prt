﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public abstract class AbstractNode : InfrastructureElement, IComparable<AbstractNode>
    {
        protected AbstractNode(Color defColor, Color hlightColor, Point pos, bool isStationElement,
                               Dictionary<Point, InfrastructureElement> elements)
            : base(defColor, pos)
        {
            HighlightedColor = hlightColor;
            IncomingRoutes = new List<AbstractRoute>();
            OutgoingRoutes = new List<AbstractRoute>();
            Occupied = false;
            _isStationElement = isStationElement;
            PDistance = Pathfinder.Infinity;

            if (!IsValidNode(Pos, elements))
            {
                // although the place is occupied, we are trying to be smart and create node inside a route
                // but such behavior is inacceptable for internal station nodes
                // (for external too, as creating a station can fail and we won't recover the broken route)
                if (_isStationElement)
                    throw new Exception("A route is in the path.");
                // check if we can be inserted into an existing route
                var clickedRouteElement = elements[Pos] as RouteElement;
                if (clickedRouteElement == null)
                    throw new Exception("That position " + Pos.ToString() + " is occupied.");
                if (clickedRouteElement.IsStationElement())
                    throw new Exception("That element belongs to a station and cannot be modified.");
                AbstractNode routeBegin = clickedRouteElement.Route.StartNode;
                AbstractNode routeEnd = clickedRouteElement.Route.EndNode;
                // new routes will inherit settings from old route
                AbstractRoute dividedRoute = clickedRouteElement.Route;
                clickedRouteElement.DeleteMe(elements);
                new Route(routeBegin, this, elements, dividedRoute);
                new Route(this, routeEnd, elements, dividedRoute);
            }
            
            elements.Add(Pos, this);
        }

        [OnSerializing]
        private void SaveSerializedColor(StreamingContext context)
        {
            _serializedHighlightColor = HighlightedColor.ToString();
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext context)
        {
            // set default values for backwards save compatibility
            HasTrafficLight = false;
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            HighlightedColor = ParseColor(_serializedHighlightColor);
            PDistance = Pathfinder.Infinity;
        }


        public override string ToString()
        {
            String result = base.ToString();
            result += "\nIncoming routes: ";
            result = IncomingRoutes.Aggregate(result, 
                (current, incomingRoute) => current + incomingRoute.Direction.OppositeDirection() + " ");
            result += "\nOutgoing routes: ";
            result = OutgoingRoutes.Aggregate(result,
                (current, outgoingRoute) => current + outgoingRoute.Direction + " ");
            return result;
        }

        // loses all references to adjacent routes and their children
        // loses the reference to us from Elements
        public override void DeleteMe(Dictionary<Point, InfrastructureElement> elements)
        {
            while (IncomingRoutes.Any())
                IncomingRoutes.Last().DeleteMe(elements);
            while (OutgoingRoutes.Any())
                OutgoingRoutes.Last().DeleteMe(elements);
            elements.Remove(Pos);
        }

        protected static bool IsValidNode(Point p, Dictionary<Point, InfrastructureElement> elements)
        {
            return !elements.ContainsKey(p);
        }

        // can it be an end of a route made by user
        public virtual bool IsUserLinkable()
        {
            return true;
        }

        public override sealed bool IsStationElement()
        {
            // in this part of tree hierarchy, the information is kept in a field
            // (because it is needed in constructor)
            return _isStationElement;
        }

        public abstract bool IsPathfinderCrossable();

        public override List<AbstractRoute> GetAdjacentRoutes()
        {
            var result = new List<AbstractRoute>(IncomingRoutes);
            result.AddRange(OutgoingRoutes);
            return result;
        }

        // called when a vehicle reaches us and we are its destination
        // should prepare a new destination for vehicle and set if the vehicle is active
        public virtual void VehicleReachedDestination(Vehicle movingVehicle)
        {
            Debug.Assert(false); // a typical node shouldn't ever be the destination
        }

        public override InfrastructureElement MakeVehicleMove(Vehicle movingVehicle, ref int usedDistance)
        {
            Debug.Assert(movingVehicle.Active);
            // if we have run of of move points, end moving
            if (usedDistance >= movingVehicle.MovePointsThisTick)
                return this;
            // if we are at the node, now we must decide which route to go through
            var routes = movingVehicle.Pathfinder.NextRoutes(this, movingVehicle.Destination, movingVehicle);
            Debug.Assert(routes != null); // should be detected before we went on a journey
            if (!routes.Any())
            {
                Debug.Assert(movingVehicle.Destination == this);
                var lastDestination = movingVehicle.Destination;
                VehicleReachedDestination(movingVehicle);
                if (lastDestination != movingVehicle.Destination && movingVehicle.Active)
                    return MakeVehicleMove(movingVehicle, ref usedDistance); // recursive call
                return this; // vehicle stopped
            }
            var nextRoute = routes.First.Value;
            Debug.Assert(OutgoingRoutes.Contains(nextRoute));
            if (!nextRoute.CanEnter())
                return this;
            routes.RemoveFirst();
            // we don't change the next location of vehicle yet - the route will do it (maybe the route is empty...)
            return nextRoute.MakeVehicleMove(movingVehicle, ref usedDistance);
        }

        public override void ClearSimulation()
        {
            base.ClearSimulation();
            Occupied = false;
            HasTrafficLight = false;
        }

        public override void ClearAfterTick()
        {
            base.ClearAfterTick();
            Occupied = Vehicle != null;
            if (HasTrafficLight)
            {
                --TicksToSwitchLight;
                if (TicksToSwitchLight <= 0)
                {
                    TicksToSwitchLight = TrafficLightPeriod;
                    GreenLightRouteIndex = (GreenLightRouteIndex + 1)%IncomingRoutes.Count;
                }
            }
        }

        // true if any vehicle from fromRoute can reach this node during this tick
        protected bool CanAnythingReachNode(AbstractRoute fromRoute)
        {
            int vehiclesDistance = 1;
            for (int i = fromRoute.RouteElements.Length - 1; i >= 0; --i, ++vehiclesDistance) {
                var privilegedVehicle = fromRoute.GetVehicle(i);
                if (privilegedVehicle == null)
                    continue;
                if (Math.Min(privilegedVehicle.MovePointsThisTick, fromRoute.VMax) >= vehiclesDistance)
                    return true;
                return false; // last vehicle on the route can't reach us, so nothing on this route can
            }
            return false;
        }

        // checks if the node isn't visited this tick by another vehicle
        // if returns true, makes also reservation
        public bool CanEnter(AbstractRoute fromRoute)
        {
            if (Broken || Occupied)
                return false;
            if (HasTrafficLight && fromRoute != IncomingRoutes[GreenLightRouteIndex])
            {
                // red light
                // but we can be smart and enter the node if nothing on green light can
                if (CanAnythingReachNode(IncomingRoutes[GreenLightRouteIndex]))
                    return false;
                // else - we can enter
            }
            if (!HasTrafficLight)
            {
                Direction rightHandDirection = fromRoute.Direction.Clone();
                rightHandDirection.RotateLeft();
                var rightHandRoute =
                    IncomingRoutes.Find(incomingRoute => incomingRoute.Direction.Dir == rightHandDirection.Dir);
                if (rightHandRoute != null && CanAnythingReachNode(rightHandRoute))
                    return false;
            }
            Occupied = true;
            return true;
        }

        private String _serializedHighlightColor;
        [NonSerialized]
        private Color _highlightedColor;
        public Color HighlightedColor
        {
            get { return _highlightedColor; }
            protected set { _highlightedColor = value; }
        }

        public List<AbstractRoute> IncomingRoutes { get; protected set; }
        public List<AbstractRoute> OutgoingRoutes { get; protected set; }

        // false if a vehicle has used this node during this tick
        public bool Occupied { get; set; }

        protected bool _hasTrafficLight;

        /// <summary>
        /// set this to true/false if you want to have traffic light on junction
        /// </summary>
        public virtual bool HasTrafficLight
        {
            get { return _hasTrafficLight; }
            set
            {
                GreenLightRouteIndex = 0;
                _hasTrafficLight = value;
            }
        }

        // the incoming route that has green light (if null, no route has yet)
        public int GreenLightRouteIndex { get; private set; }

        private int _trafficLightPeriod;

        /// <summary>
        /// number of ticks after which the light will switch from green to red
        /// </summary>
        public int TrafficLightPeriod
        {
            get { return _trafficLightPeriod; }
            set
            {
                TicksToSwitchLight = value;
                _trafficLightPeriod = value;
            }
        }

        public int TicksToSwitchLight { get; private set; }

        protected bool _isStationElement;

        // ----------------
        // for the pathfinder

        [NonSerialized]
        private int _pDistance;
        public int PDistance
        {
            get { return _pDistance; }
            set { _pDistance = value; }
        }

        [NonSerialized]
        private int _pRand;
        public int PRand
        {
            get { return _pRand; }
            set { _pRand = value; }
        }

        [NonSerialized]
        private int _pId;
        public int PId
        {
            get { return _pId; }
            set { _pId = value; }
        }

        [NonSerialized]
        private AbstractRoute _pPreviousRoute;
        public AbstractRoute PPreviousRoute
        {
            get { return _pPreviousRoute; }
            set { _pPreviousRoute = value; }
        }

        public int CompareTo(AbstractNode other)
        {
            if (PDistance == other.PDistance) {
                if (PRand == other.PRand) {
                    return PId - other.PId;
                }
                return PRand - other.PRand;
            }
            return PDistance - other.PDistance;
        }
    }
}
