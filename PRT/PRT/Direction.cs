﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class Direction
    {
        public const int N = 0;
        public const int W = 1;
        public const int S = 2;
        public const int E = 3;

        public Direction(int dir)
        {
            Dir = dir;
        }

        public Direction Clone()
        {
            return new Direction(Dir);
        }

        public Direction(Point p1, Point p2)
        {
            if (p1 == p2)
                throw new InvalidOperationException("Points are the same");
            if (!InLine(p1, p2))
                throw new InvalidOperationException("Points are not inline");
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            if (dx > 0)
                Dir = E;
            else if (dx < 0)
                Dir = W;
            else if (dy > 0)
                Dir = S;
            else
                Dir = N;
        }

        public static bool InLine(Point p1, Point p2)
        {
            return (p1.X - p2.X) * (p1.Y - p2.Y) == 0;
        }

        public void RotateLeft()
        {
            Dir = (Dir + 1) % 4;
        }

        public void RotateRight()
        {
            Dir = (Dir + 4 - 1) % 4;
        }

        public Direction OppositeDirection()
        {
            return new Direction((Dir + 2)%4);
        }

        public override string ToString()
        {
            switch (Dir)
            {
                case N:
                    return "North";
                case W:
                    return "West";
                case S:
                    return "South";
                case E:
                    return "East";
            }
            throw new InvalidEnumArgumentException();
        }

        public int Dir { get; set; }
    }
}
