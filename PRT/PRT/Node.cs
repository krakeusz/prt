﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    // a junction, maybe belongs to a station
    [Serializable]
    public class Node : AbstractNode
    {
        public Node(Point pos, Dictionary<Point, InfrastructureElement> elements, bool isStationElement = false)
            : base(Colors.Blue, Colors.MidnightBlue, pos, isStationElement, elements)
        {
        }

        public override string Name
        {
            get { return "Node"; }
        }

        public override bool IsPathfinderCrossable()
        {
            return true;
        }

    }
}
