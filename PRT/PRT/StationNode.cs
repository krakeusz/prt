﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    // any node inside station
    [Serializable]
    public abstract class StationNode : Node
    {
        protected StationNode(Point pos, AbstractStation station,
                              Dictionary<Point, InfrastructureElement> elements)
            : base(pos, elements, true)
        {
            Station = station;
        }

        public override void DeleteMe(Dictionary<Point, InfrastructureElement> elements)
        {
            Station.DeleteMe(elements);
        }

        // deletes also adjacent routes, but not the whole station
        public void DeleteOnlyMe(Dictionary<Point, InfrastructureElement> elements)
        {
            base.DeleteMe(elements);
        }

        public override AbstractStation GetStation()
        {
            return Station;
        }

        public AbstractStation Station { get; set; }
    }
}
