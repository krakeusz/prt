﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    [Serializable]
    public abstract class InfrastructureElement
    {
        protected InfrastructureElement(Color color, Point pos)
        {
            Color = DefaultColor = color;
            Pos = pos.Clone();
            _vehicle = null;
        }

        [OnSerializing]
        private void SaveSerializedColor(StreamingContext context)
        {
            _serializedColor = DefaultColor.ToString();
        }

        [OnDeserializing]
        private void LoadDefaults(StreamingContext context)
        {
            Broken = false;
        }

        [OnDeserialized]
        private void LoadSerializedColor(StreamingContext context)
        {
            Color = DefaultColor = ParseColor(_serializedColor);
        }

        public abstract String Name { get; }

        public override string ToString()
        {
            return Name + ", position: " + Pos + "\nStation: " + (IsStationElement() ? "yes" : "no");
        }

        public abstract void DeleteMe(Dictionary<Point, InfrastructureElement> elements);

        public virtual void ClearSimulation()
        {
            Broken = false;
            Vehicle = null;
        }

        public virtual void ClearAfterTick()
        {
            if (Broken) {
                if (--TicksToRepair <= 0)
                    Broken = false;
            }
        }

        public virtual bool IsDepot()
        {
            return false;
        }

        public virtual bool IsStationElement()
        {
            return false;
        }

        public virtual AbstractStation GetStation()
        {
            return null;
        }

        public abstract List<AbstractRoute> GetAdjacentRoutes(); 

        // checks the vehicle can move from this element this tick
        // returns the destination
        public abstract InfrastructureElement MakeVehicleMove(Vehicle movingVehicle, ref int usedDistance);

        private String _serializedColor;

        [NonSerialized]
        protected Color _color;
        public Color Color
        {
            get
            {
                if (Broken)
                    return Colors.LawnGreen;
                if (Vehicle == null)
                    return _color;
                return Vehicle.Color;
            }
            set { _color = value; }
        }

        [NonSerialized]
        private Color _defaultColor;
        public Color DefaultColor
        {
            get { return _defaultColor; }
            protected set { _defaultColor = value; }
        }

        public Point Pos { get; set; }

        [NonSerialized]
        protected Vehicle _vehicle;
        public virtual Vehicle Vehicle
        {
            get { return _vehicle; }
            set
            {
                Debug.Assert(_vehicle == null || value == null);
                _vehicle = value;
            }
        }

        [NonSerialized]
        private bool _broken;
        public bool Broken
        {
            get { return _broken; }
            set
            {
                if (!_broken && value) {
                    foreach (var adjacentRoute in GetAdjacentRoutes()) {
                        ++adjacentRoute.BrokenElements;
                    }
                } else if (_broken && !value) {
                    foreach (var adjacentRoute in GetAdjacentRoutes()) {
                        --adjacentRoute.BrokenElements;
                    }
                }
                _broken = value;
            }
        }

        [NonSerialized]
        private int _ticksToRepair;
        public int TicksToRepair
        {
            get { return _ticksToRepair; }
            set { _ticksToRepair = value; }
        }

        protected Color ParseColor(string hex)
        {
            try {
                byte[] parsedColor = ParseHex(hex);
                return Color.FromArgb(parsedColor[0], parsedColor[1], parsedColor[2], parsedColor[3]);
            } catch (Exception e) {
                throw new SerializationException("Invalid color: " + _serializedColor, e);
            }
            
        }

        private byte[] ParseHex(string hex)
        {
            int offset = hex.StartsWith("#") ? 1 : 0;
            if (((hex.Length - offset) % 2) != 0) {
                throw new ArgumentException("Invalid length: " + hex.Length);
            }
            byte[] ret = new byte[(hex.Length - offset) / 2];

            for (int i = 0; i < ret.Length; i++) {
                ret[i] = (byte)((ParseNybble(hex[offset]) << 4)
                                 | ParseNybble(hex[offset + 1]));
                offset += 2;
            }
            return ret;
        }

        private int ParseNybble(char c)
        {
            if (c >= '0' && c <= '9') {
                return c - '0';
            }
            if (c >= 'A' && c <= 'F') {
                return c - 'A' + 10;
            }
            if (c >= 'a' && c <= 'f') {
                return c - 'a' + 10;
            }
            throw new ArgumentException("Invalid hex digit: " + c);
        }

    }
}
