﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PRT
{
    public class Vehicle
    {
        public Vehicle(Depot depot, Pathfinder pathfinder, int capacity, int vmax, int a)
        {
            _location = depot;
            Destination = null;
            Pathfinder = pathfinder.Clone(this);
            Capacity = capacity;
            Passengers = 0; // after capacity
            PassengersDestination = null;
            DepotDestination = null;
            CalledToStation = null;
            LastPassengerNode = null;
            QueuedPassengers = 0;
            VMax = vmax;
            A = a;
            V = 0;
            Active = false;
            depot.StoppedVehicles.Enqueue(this);
        }

        public bool IsInDepot()
        {
            return Location.Vehicle != this;
        }

        public void DeleteMe()
        {
            Location.Vehicle = null;
        }

        public void ClearAfterTick()
        {
            if (Broken && --TicksToRepair <= 0)
                Broken = false;
            MovePointsThisTick = Math.Min(V + A, VMax);
        }

        public void SetMaxVelocityThisTick(int max)
        {
            if (max < MovePointsThisTick)
                MovePointsThisTick = max;
        }

        // returns the element we will end this tick on
        // null if no move
        public InfrastructureElement MakeMove()
        {
            int usedDistance = 0;
            var endLocation = Location.MakeVehicleMove(this, ref usedDistance);
            Debug.Assert(usedDistance <= Math.Min(V + A, VMax));
            var endRouteElement = endLocation as RouteElement;
            if (endRouteElement != null && endRouteElement.Route.VMax < V)
            {
                V = endRouteElement.Route.VMax;
            }
            else {
                V = IsDocked ? 0 : usedDistance;
            }
            if (endLocation != Location)
                return endLocation;
            return null;
        }

        private InfrastructureElement _location;
        /// <summary>
        /// != null
        /// </summary>
        public InfrastructureElement Location
        {
            get { return _location; }
            set
            {
                if (value == _location) return;
                if (_location != null)
                {
                    _location.Vehicle = null;
                }
                _location = value;
                Debug.Assert(value != null);
                value.Vehicle = this;
            }
        }

        private Color _color;
        public Color Color
        {
            get
            {
                if (Broken)
                    return Colors.Gray;
                return _color;
            }
            private set { _color = value; }
        }

        public AbstractNode Destination { get; set; }
        public Pathfinder Pathfinder { get; set; }
        private int _passengers;
        public int Passengers 
        { 
            get { return _passengers; }
            set
            {
                _passengers = value;
                byte rgbValue = (byte) (255 - 255 * _passengers / Capacity);
                var vehicleBrush = Color.FromArgb(255, rgbValue, 0, 0);
                Color = vehicleBrush;
            }
        }
        // station that the passengers will travel to
        public AbstractStation PassengersDestination { get; set; }
        // if not null, then the vehicle is heading towards a depot
        // takes precedence over PassengersDestination
        public Depot DepotDestination { get; set; }
        // if the vehicle was called by station and is on its way to it, points to that station
        public AbstractStation CalledToStation { get; set; }
        // passengers waiting to enter the vehicle
        public int QueuedPassengers { get; set; }
        public PassengerNode LastPassengerNode { get; set; }
        public int Capacity { get; set; }
        public int A { get; set; }
        public int V { get; set; }
        public int MovePointsThisTick { get; set; }
        public int VMax { get; set; }
        public bool IsDocked { get; set; }

        // true if the vehicle can move this turn
        // false if in depot or passengerNode
        // doesn't take Broken into account
        public bool Active { get; set; }

        private bool _broken;
        public bool Broken
        {
            get { return _broken; }
            set
            {
                if (!_broken && value) {
                    foreach (var adjacentRoute in Location.GetAdjacentRoutes()) {
                        ++adjacentRoute.BrokenElements;
                    }
                } else if (_broken && !value) {
                    foreach (var adjacentRoute in Location.GetAdjacentRoutes()) {
                        --adjacentRoute.BrokenElements;
                    }
                }
                _broken = value;
                V = 0;
            }
        }

        public int TicksToRepair { get; set; }
    }
}
