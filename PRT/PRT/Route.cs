﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    class Route : AbstractRoute
    {
        public Route(AbstractNode begin, AbstractNode end, Dictionary<Point, InfrastructureElement> elements,
                     AbstractRoute patternRoute = null)
            : base(begin, end, elements, RouteElement.Instance, patternRoute)
        {
        }

    }
}
