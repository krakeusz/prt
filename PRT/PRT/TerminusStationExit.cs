﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class TerminusStationExit : StationExit
    {
        public TerminusStationExit(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
        }
    }
}
