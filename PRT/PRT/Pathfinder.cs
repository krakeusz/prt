﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    public abstract class Pathfinder
    {
        public const int Infinity = 1000000000;

        protected Pathfinder(String name)
        {
            Name = name;
        }

        public abstract Pathfinder Clone(Vehicle newVehicle);

        // should return a list of routes on a path to goal, excluding the one we're on, if any
        // can be incomplete, but must contain at least one route
        // once the list is returned, that will be the next path of a vehicle
        // can be called many times, even if we haven't gone through the previously returned list
        // but the list can be shortened outside, when we traverse a route
        // returns empty list, when we are at the destination
        // returns null when the destination is unreachable
        public LinkedList<AbstractRoute> NextRoutes(AbstractNode startNode, AbstractNode destinationNode,
                                                    Vehicle vehicle)
        {
            LinkedList<AbstractRoute> result;
            List<AbstractNode> ignore;
            DijkstraBase(startNode, destinationNode, vehicle, null,
                         OutgoingRoutes, StartNode, EndNode, 
                         out result, out ignore);
            return result;
        }

        // returns list of reachable nodes of type endNodeType
        public List<AbstractNode> NearestNodes(AbstractNode startNode, Type endNodeType,
                                                     bool isTransponed)
        {
            OutgoingRoutesDelegate outgoingRoutesDelegate;
            StartNodeDelegate startNodeDelegate;
            EndNodeDelegate endNodeDelegate;
            if (isTransponed)
            {
                outgoingRoutesDelegate = IncomingRoutes;
                startNodeDelegate = EndNode;
                endNodeDelegate = StartNode;
            }
            else
            {
                outgoingRoutesDelegate = OutgoingRoutes;
                startNodeDelegate = StartNode;
                endNodeDelegate = EndNode;
            }
            LinkedList<AbstractRoute> ignore;
            List<AbstractNode> result;
            DijkstraBase(startNode, null, null, endNodeType,
                         outgoingRoutesDelegate, startNodeDelegate, endNodeDelegate,
                         out ignore, out result);
            return result;
        }

        public override string ToString()
        {
            return Name;
        }

        // if destinationNode != null, search for shortest path to it
        // else, search for all reachable nodes of type endNodeType, and return them in order of distance
        // (including startNode)
        protected virtual void DijkstraBase(AbstractNode startNode, AbstractNode destinationNode, Vehicle vehicle,
                                            Type endNodeType,
                                            OutgoingRoutesDelegate outgoingRoutesDelegate,
                                            StartNodeDelegate startNodeDelegate,
                                            EndNodeDelegate endNodeDelegate,
                                            out LinkedList<AbstractRoute> nextRoutes,
                                            out List<AbstractNode> nearestNodes)
        {
            var Q = new SortedDictionary<AbstractNode, AbstractNode>();
            var visitedNodes = new List<AbstractNode>();
            nearestNodes = null;
            nextRoutes = null;
            if (destinationNode == null)
                nearestNodes = new List<AbstractNode>();
            startNode.PDistance = 0;
            int id = 0;
            startNode.PId = id++;
            startNode.PRand = Random.Instance.Next();
            startNode.PPreviousRoute = null;
            Q.Add(startNode, null);
            while (Q.Any()) {
                var currentNode = Q.First().Key;
                Q.Remove(currentNode);
                visitedNodes.Add(currentNode);
                if (currentNode == destinationNode) {
                    // we are searching for route path
                    nextRoutes = new LinkedList<AbstractRoute>();
                    while (currentNode.PPreviousRoute != null) {
                        nextRoutes.AddFirst(currentNode.PPreviousRoute);
                        currentNode = startNodeDelegate(currentNode.PPreviousRoute);
                    }
                    RestoreNodes(visitedNodes);
                    return;
                }
                if (destinationNode == null && endNodeType.IsInstanceOfType(currentNode))
                {
                    // we are searching for reachable nodes
                    nearestNodes.Add(currentNode);
                }
                if (!currentNode.IsPathfinderCrossable() && currentNode != startNode)
                    continue;
                foreach (var outgoingRoute in outgoingRoutesDelegate(currentNode)) {
                    var routeCost = EstimatedCost(outgoingRoute, vehicle);
                    var endNode = endNodeDelegate(outgoingRoute);
                    if (currentNode.PDistance + routeCost < endNode.PDistance) {
                        if (Q.Remove(endNode) == false) {
                            // element wasn't in Queue
                            endNode.PId = id++;
                            endNode.PRand = Random.Instance.Next();
                            visitedNodes.Add(endNode);
                        }
                        endNode.PPreviousRoute = outgoingRoute;
                        endNode.PDistance = currentNode.PDistance + routeCost;
                        Q.Add(endNode, null);
                    }
                }
            }
            RestoreNodes(visitedNodes);
        }

        protected void RestoreNodes(IEnumerable<AbstractNode> nodes)
        {
            foreach (var node in nodes) {
                node.PDistance = Infinity;
            }
        }

        protected abstract int EstimatedCost(AbstractRoute route, Vehicle vehicle);

        protected delegate List<AbstractRoute> OutgoingRoutesDelegate(AbstractNode node);

        protected List<AbstractRoute> OutgoingRoutes(AbstractNode node)
        {
            return node.OutgoingRoutes;
        } 

        protected List<AbstractRoute> IncomingRoutes(AbstractNode node)
        {
            return node.IncomingRoutes;
        }

        protected delegate AbstractNode StartNodeDelegate(AbstractRoute route);

        protected AbstractNode StartNode(AbstractRoute route)
        {
            return route.StartNode;
        }

        protected delegate AbstractNode EndNodeDelegate(AbstractRoute route);

        protected AbstractNode EndNode(AbstractRoute route)
        {
            return route.EndNode;
        }   

        protected Vehicle Vehicle { get; set; }
        protected String Name { get; set; }
    }

    class StaticDijkstraPathfinder : Pathfinder
    {
        public StaticDijkstraPathfinder(Vehicle newVehicle)
            : base("Static Dijkstra")
        {
            Vehicle = newVehicle;
        }

        public override Pathfinder Clone(Vehicle newVehicle)
        {
            return new StaticDijkstraPathfinder(newVehicle);
        }

        protected override int EstimatedCost(AbstractRoute route, Vehicle vehicle)
        {
            // basically it is the length of the route
            // but if maximum speed on the route is lower, the cost is higher
            int vehicleVMax = vehicle == null ? route.VMax : vehicle.VMax;
            return route.RouteElements.Length * vehicleVMax / Math.Min(route.VMax, vehicleVMax);
        }
    }

    class DynamicDijkstraPathfinder : Pathfinder
    {
        public DynamicDijkstraPathfinder(Vehicle newVehicle)
            : base("Dynamic Dijkstra")
        {
            Vehicle = newVehicle;
        }

        public override Pathfinder Clone(Vehicle newVehicle)
        {
            return new DynamicDijkstraPathfinder(newVehicle);
        }

        protected override int EstimatedCost(AbstractRoute route, Vehicle vehicle)
        {
            // length of the route or more (when there are vehicles on the route)
            int vehicleVMax = vehicle == null ? route.VMax : vehicle.VMax;
            int baseLength = route.RouteElements.Length*vehicleVMax/Math.Min(route.VMax, vehicleVMax);
            if (route.BrokenElements > 0)
                baseLength += SimulationSettings.BrokenPeriod*vehicleVMax;
            return (int) (baseLength * (1.0 + route.VehicleCount * 8.0/route.RouteElements.Length));
        }
    }
}
