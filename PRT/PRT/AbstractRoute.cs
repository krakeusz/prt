﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PRT
{
    [Serializable]
    public abstract class AbstractRoute
    {
        /* Route has (length) elements
         * element #0 is null as it belongs to a Node
         * element #1 is the first valid RouteElement (if length >= 2)
         * element #(length - 1) is the last valid RouteElement (just before a crossroad)
         * if dictionary 'elements' is null, don't add visual elements
         * 
         * @param patternElement the RouteElement that will be cloned into our RouteElements
         * @param patternRoute the Route from which we will take VMax and MinSeparation, or default if null
         */
        protected AbstractRoute(AbstractNode begin, AbstractNode end, Dictionary<Point, InfrastructureElement> elements,
                                RouteElement patternElement, AbstractRoute patternRoute = null)
        {
            Point[] path;
            if (!IsValidRoute(begin.Pos, end.Pos, out path, elements))
                throw new Exception("Invalid route");

            StartNode = begin;
            EndNode = end;
            RouteElements = new RouteElement[Math.Abs(StartNode.Pos.X - EndNode.Pos.X)
                                             + Math.Abs(StartNode.Pos.Y - EndNode.Pos.Y)];
            
            for (int i = 1; i < RouteElements.Length; ++i)
            {
                var newRouteElement = patternElement.Create(i, path[i - 1], this);
                RouteElements[i] = newRouteElement;
                if (elements != null)
                    elements.Add(newRouteElement.Pos, newRouteElement);
            }
            StartNode.OutgoingRoutes.Add(this);
            EndNode.IncomingRoutes.Add(this);
            if (patternRoute != null)
            {
                VMax = patternRoute.VMax;
                MinSeparation = patternRoute.MinSeparation;
            }
            else
            {
                VMax = DrawSettings.Instance.RouteVMax;
                MinSeparation = DrawSettings.Instance.RouteMinSeparation;
            }
            Direction = new Direction(StartNode.Pos, EndNode.Pos);
            VehicleCount = 0;
        }

        [OnDeserializing]
        protected void OnDeserializing(StreamingContext context)
        {
            BrokenElements = 0;
        }

        public override string ToString()
        {
            return "Route direction: " + Direction  + "\n"
                + "V max: " + VMax + "\nMin separation: " + MinSeparation;
        }

        // removes all references to RouteElements
        // removes references to us from adjacent edges
        public virtual void DeleteMe(Dictionary<Point, InfrastructureElement> elements)
        {
            for (int i = 1; i < RouteElements.Length; ++i) {
                RouteElements[i].Route = null;
                elements.Remove(RouteElements[i].Pos);
                RouteElements[i] = null;
            }
            StartNode.OutgoingRoutes.Remove(this);
            EndNode.IncomingRoutes.Remove(this);
        }


        // if it returns true, it outs a path of squares for the route (excluding start/end nodes)
        // if elements == null, don't check for visual elements disjunction
        public static bool IsValidRoute(Point p1, Point p2, out Point[] path, Dictionary<Point, InfrastructureElement> elements = null)
        {
            int dx = (p2.X - p1.X);
            int dy = (p2.Y - p1.Y);
            path = null;
            if (dx == 0 && dy == 0) {
                return false;
            }
            if (!Direction.InLine(p1, p2)) {
                // not strictly vertical/horizontal route
                return false;
            }
            int stepX = Math.Sign(dx);
            int stepY = Math.Sign(dy);
            int x = p1.X + stepX;
            int y = p1.Y + stepY;
            int pathLength = Math.Abs(dx) + Math.Abs(dy) - 1;
            if (pathLength == 0 && elements != null)
            {
                // we wouldn't capture the case where there are two routes between the same nodes
                InfrastructureElement el1, el2;
                AbstractNode n1, n2;
                if (elements.TryGetValue(p1, out el1) && elements.TryGetValue(p2, out el2))
                {
                    n1 = el1 as AbstractNode;
                    n2 = el2 as AbstractNode;
                    if (n1 != null && n2 != null)
                    {
                        if ((from outgoingRoute in n1.OutgoingRoutes from incomingRoute in n2.IncomingRoutes
                             where outgoingRoute == incomingRoute select outgoingRoute).Any())
                        {
                            return false;
                        }
                    }
                }
            }
            path = new Point[pathLength];
            for (int i = 0; i < pathLength; ++i) {
                path[i] = new Point(x, y);
                if (elements != null && elements.ContainsKey(path[i])) {
                    path = null;
                    return false;
                }
                x += stepX;
                y += stepY;
                if (x == p2.X && y == p2.Y)
                    break;
            }
            return true;
        }

        public virtual bool IsStationRoute()
        {
            return false;
        }

        public virtual AbstractStation GetStation()
        {
            return null;
        }

        // 0 (!) <= index <= (!) Route.Length
        public bool IsVehicleThere(int index)
        {
            return GetVehicle(index) != null;
        }

        // 0 (!) <= index <= (!) Route.Length
        public Vehicle GetVehicle(int index)
        {
            if (index == RouteElements.Length)
                return EndNode.Vehicle;
            if (index == 0)
                return StartNode.Vehicle;
            return RouteElements[index].Vehicle;
        }

        // 1 <= index <= Route.Length
        // for nodes, checks if we can enter
        public bool IsRouteBlocked(int index)
        {
            if (index == RouteElements.Length)
                return !EndNode.CanEnter(this);
            return RouteElements[index].Broken;
        }

        public bool CanEnter()
        {
            for (int i = 1; i <= Math.Min(MinSeparation + 1, RouteElements.Length); ++i) {
                if (IsVehicleThere(i))
                    return false; // we can't move, because we would be too close to another vehicle
            }
            return true;
        }

        public InfrastructureElement MakeVehicleMove(Vehicle movingVehicle, ref int usedDistance)
        {
            // assuming that the virtual position of vehicle is at the StartNode
            // we can't stop at the route, only at node or route element
            Debug.Assert(usedDistance < movingVehicle.MovePointsThisTick); // there are move points left
            Debug.Assert(CanEnter());
            InfrastructureElement nextCallElement;
            if (RouteElements.Length == 1)
            {
                if (EndNode.CanEnter(this))
                {
                    ++usedDistance;
                    nextCallElement = EndNode;
                }
                else
                {
                    return StartNode;
                }
            }
            else
            {
                if (RouteElements[1].Broken)
                    return StartNode;
                ++usedDistance;
                nextCallElement = RouteElements[1];
            }
            if (usedDistance >= this.VMax)
            {
                movingVehicle.SetMaxVelocityThisTick(this.VMax);
                return nextCallElement; // stop now
            }
            return nextCallElement.MakeVehicleMove(movingVehicle, ref usedDistance);
        }

        public AbstractNode StartNode { get; private set; }
        public AbstractNode EndNode { get; private set; }
        public RouteElement[] RouteElements { get; private set; }

        public int VMax { get; set; }
        // amount of free space between any two vehicles on a route (doesn't apply over junctions)
        public int MinSeparation { get; set; }
        public Direction Direction { get; set; }
        // number of vehicles on a route
        public int VehicleCount { get; set; }
        // number of broken route elements
        public int BrokenElements { get; set; }

    }
}
