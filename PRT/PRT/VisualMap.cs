﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace PRT
{
    public class VisualMap : FrameworkElement
    {

        public VisualMap(StatusBarItem info)
        {
            DefaultSquareSize = SquareSize = 10;
            DrawSettings.Instance.Info = info;
            DrawSettings.Instance.VisualMap = this;
            _layers = new VisualCollection(this) {new DrawingVisual()};
            Map = new Map();
            RenderTransform = new MatrixTransform();
        }

        public void DrawMap()
        {
            var layer = _layers[0] as DrawingVisual;
            Debug.Assert(layer != null);
            var drawingContext = layer.RenderOpen();
            foreach (var kv in Map.Elements)
            {
                var visPoint = AbsToVis(kv.Key.ToWindowsPoint());
                drawingContext.DrawRectangle(new SolidColorBrush(kv.Value.Color), null,
                    new Rect(visPoint.X + 1, visPoint.Y + 1, SquareSize - 1, SquareSize - 1));
            }
            drawingContext.Close();
        }

        public void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Point clickedPoint = e.GetPosition((UIElement)sender);
            var p = Point.FromWindowsPoint(VisToAbs(clickedPoint));
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    if (Simulation == null)
                    {
                        Map.OnMouseLeftButtonUp(p);
                    }
                    else {
                        Simulation.OnMouseLeftButtonUp(p);
                    }
                    break;
                case MouseButton.Right:
                    Map.OnMouseRightButtonUp(p);
                    break;
                case MouseButton.Middle:
                    OnScrollMouseUp(sender, e);
                    Map.OnMouseMiddleButtonUp(p);
                    break;
            }
        }

        public void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
                OnScrollMouseDown(sender, e);
        }

        private void OnScrollMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsMousePressed = true;
            LastMousePressedPoint = e.GetPosition((UIElement)sender);
        }

        private void OnScrollMouseUp(object sender, MouseButtonEventArgs e)
        {
            IsMousePressed = false;
        }

        private void TranslateMatrix(double dx, double dy)
        {
            var newMatrix = RenderTransform.Value;
            newMatrix.Translate(dx, dy);
            RenderTransform = new MatrixTransform(newMatrix);
        }

        private void OnScrollMouseMove(object sender, MouseEventArgs e)
        {
            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                var thisPoint = e.GetPosition((UIElement)sender);
                TranslateMatrix(thisPoint.X - LastMousePressedPoint.X, thisPoint.Y - LastMousePressedPoint.Y);
                LastMousePressedPoint = thisPoint;
                DrawMap();
            }
        }

        public void OnMouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Point clickedPoint = e.GetPosition((UIElement)sender);
            var p = Point.FromWindowsPoint(VisToAbs(clickedPoint));
            OnScrollMouseMove(sender, e);
            switch(DrawSettings.Instance.State)
            {
                case DrawSettings.EState.RouteEnd:
                case DrawSettings.EState.AutoRouteEnd:
                    if (DrawSettings.Instance.HighlightedRouteStart != null)
                    {
                        DrawSettings.Instance.IsRouteInLine = Direction.InLine(p,DrawSettings.Instance.HighlightedRouteStart.Pos);
                    }
                    break;
            }
        }

        public void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var mousePointBefore = e.GetPosition((UIElement)sender);
            var squarePoint = VisToAbs(mousePointBefore);
            SquareSize += Math.Sign(e.Delta);
            if (SquareSize < 3)
                SquareSize = 3;
            var mousePointAfter = AbsToVis(squarePoint, true);
            // translate so that the mouse point will be the same after zoom
            TranslateMatrix((int)(mousePointBefore.X - mousePointAfter.X), (int)(mousePointBefore.Y - mousePointAfter.Y));
            DrawMap();
        }

        public void ClearTransforms()
        {
            SquareSize = DefaultSquareSize;
            RenderTransform = new MatrixTransform();
            DrawMap();
        }

        /* 1. Vis = point clicked on screen in base (1, 1)
         * 2. Mid = Vis after inverse transform (translated) in base(1, 1)
         * 3. Abs = Mid in base (SquareSize, SquareSize)
         */

        // by default, doesn't transform (returns Mid coordinates)
        private System.Windows.Point AbsToVis(System.Windows.Point abs, bool transformToo = false)
        {
            var result = new System.Windows.Point(abs.X*SquareSize, abs.Y*SquareSize);
            if (transformToo)
                result = RenderTransform.Transform(result);
            return result;
        }

        private System.Windows.Point VisToAbs(System.Windows.Point vis)
        {
            var realClickedPoint = RenderTransform.Inverse.Transform(vis);
            return new System.Windows.Point(Math.Floor(realClickedPoint.X / SquareSize), Math.Floor(realClickedPoint.Y / SquareSize));
        }

        // Provide a required override for the VisualChildrenCount property.
        protected override int VisualChildrenCount
        {
            get { return _layers.Count; }
        }

        // Provide a required override for the GetVisualChild method.
        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= _layers.Count) {
                throw new ArgumentOutOfRangeException();
            }

            return _layers[index];
        }

        // currently there is only 1 layer, managed by this class
        private VisualCollection _layers;
        private bool IsMousePressed { get; set; }
        private System.Windows.Point LastMousePressedPoint { get; set; }
        private int DefaultSquareSize { get; set; }

        public int SquareSize { get; set; }

        private Map _map;
        public Map Map
        {
            get { return _map; }
            set
            {
                _map = value;
                _map.VisualMap = this;
                DrawMap();
            }
        }

        public Simulation Simulation;
    }

}
