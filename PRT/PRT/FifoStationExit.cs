﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PRT
{
    [Serializable]
    public class FifoStationExit : StationExit
    {
        public FifoStationExit(Point pos, AbstractStation station, Dictionary<Point, InfrastructureElement> elements)
            : base(pos, station, elements)
        {
        }

        public override void VehicleReachedDestination(Vehicle movingVehicle)
        {
            if (movingVehicle.LastPassengerNode != null)
            {
                movingVehicle.LastPassengerNode.Reserved = false;
                movingVehicle.LastPassengerNode = null;
            }
            base.VehicleReachedDestination(movingVehicle);
        }
    }
}
